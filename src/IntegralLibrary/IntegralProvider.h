/**
 * @file IntegralProvider.h
 *
 * @date 24.07.2019
 *
 * @brief Header file for the IntegralLibrary provider
 *
 * Here we provide an interface to the IntegralLibrary which complies with the 
 * CoefficientProvider/IntegralProvider interface described in the note:
 *
 * ~Caravel/documentation/notes/CoeffIntegralInteractions
 */

#ifndef INTEGRALPROVIDER_H_
#define INTEGRALPROVIDER_H_

#include <vector>
#include <iostream>
#include <cmath>
#include <exception>
#include <algorithm>
#include <cassert>
#include <functional>
#include <utility>
#include <memory>

#include "Core/Utilities.h"
#include "Core/type_traits_extra.h"
#include "Core/settings.h"
#include "Core/typedefs.h"

#include "Core/momD_conf.h"
#include "Graph/IntegralGraph.h"
#include "Graph/GraphKin.h"
#include "Core/Series.h"
#include "Core/Debug.h"

#include "MasterIntegralData.h"
#include "IntegralTagger.h"
#include "IntegralLibrary/integral_collection/IntegralImport.hpp"
#include "BasisElementFunction.h"
#include "FBCoefficient.h"

namespace Caravel {
namespace Integrals {

/**
 * Picks the corresponding numerical type for the input type. If we are
 * numeric then this is just the input. If we are finite field this is
 * complex.
 */
template <typename T, typename = void>
struct floating_partner;

template <typename T>
struct floating_partner<T, typename std::enable_if_t<!is_exact<T>::value>> {
    typedef T type;
};

template <typename T>
struct floating_partner<T, typename std::enable_if_t<is_exact<T>::value>> {
    typedef C type;
};

template <typename T>
using floating_partner_t = typename floating_partner<T>::type;

template <typename CType, typename FType>
class IntegralProvider {

    typedef typename FType::input_type T;	/**< The numerical type of the input to the special functions */
    typedef typename FType::output_type Out;	/**< The numerical type of the output of the special functions */

    typedef typename CType::input_type TC;	/**< The numerical type of the input to the special-function coefficients */
    typedef typename CType::output_type OutC;	/**< The numerical type of the output of the special-function coefficients */

    static bool loader;    /**< Static variable to track if integrals have been added to corresponding maps (initialized to false) */

  protected:

    lGraph::IntegralContainer integrals;	/**< Keeps information of integrals associated with cuts in hierarchy */
    std::vector<T> masses;	/**< Vector containing masses (as many as added according to ParticleMass::mass_index_global) */
    T mu_square;	/**< Regularization/renormalization scale squared */
    std::vector<std::function<Series<OutC>(const momD_conf<TC, 4>&)>> integral_series;	/**< Container with functors necessary for the compute(.) method */
    /**
     * The type that a function Caravel::Integrals::master<CType,FType>.get_basis() returns
     * when evaluated on a PS point (a set of invariants extracted from a given momD_conf)
     */
    using NFBExpansion = NumericFunctionBasisExpansion<Series<OutC>, FType>;
    std::vector<std::function<NFBExpansion(const momD_conf<TC,4>&)>> integral_nfbe;	/**< Container with functors that store an abstract representation of the master integrals that can be eval with momD_conf */
    std::vector<lGraph::IntegralGraph> integral_graphs;

  public:
    IntegralProvider() = delete;
    IntegralProvider(const std::vector<lGraph::IntegralGraph>&);

    lGraph::IntegralContainer get_integrals() const;

    std::vector<NFBExpansion> get_abstract_integral(const momD_conf<TC, 4>&) const;
    std::function<std::vector<NFBExpansion>(const momD_conf<TC, 4>&)> get_int_evaluator() const;

    void set_mu_squared(const TC& m);

    std::vector<Series<OutC>> compute(const momD_conf<TC, 4>& in) const;

  private:
    /**
     * Private method to handle integrals
     */
    void construct_integrals();
};

/**
 * Simplified integral provider: We introduce this alias template as though IntegralProvider 
 * is somehow flexible, most usage goes with a single numeric type (and the deduced one from 
 * floating_partner_t)
 */
template <typename T>
using sIntegralProvider = IntegralProvider<FBCoefficient<T, T>, StdBasisFunction<floating_partner_t<T>, floating_partner_t<T>>>;

}
}

// IMPLEMENTATIONS
#include "IntegralProvider.hpp"

#endif   // INTEGRALPROVIDER_H_
