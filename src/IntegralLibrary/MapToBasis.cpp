#include "MapToBasis.h"

namespace Caravel {
namespace Integrals {

// ----------------------------------------------------------------------------
// ----------------- Explicit Instatiations -----------------------------------
// ----------------------------------------------------------------------------
#define INSTANTIATE_MAP_TO_BASIS(T, OUTF, INF) \
    template class MapToBasis<T, StdBasisFunction<OUTF, INF>>;

INSTANTIATE_MAP_TO_BASIS(F32, C, R)
#ifdef HIGH_PRECISION
INSTANTIATE_MAP_TO_BASIS(F32, CHP, RHP)
#endif
#ifdef VERY_HIGH_PRECISION
INSTANTIATE_MAP_TO_BASIS(F32, CVHP, RVHP)
#endif
#ifdef INSTANTIATE_RATIONAL
INSTANTIATE_MAP_TO_BASIS(BigRat, C, R)
#ifdef HIGH_PRECISION
INSTANTIATE_MAP_TO_BASIS(BigRat, CHP, RHP)
#endif
#ifdef VERY_HIGH_PRECISION
INSTANTIATE_MAP_TO_BASIS(BigRat, CVHP, RVHP)
#endif
#endif

} // namespace Integrals
} // namespace Caravel
