#include "InputVariables.h"

#include <sstream>

namespace Caravel {
namespace Integrals {

std::string InputVariable::as_string() const {
    std::ostringstream oss;
    oss << wise_enum::to_string(invariant_type) << "[" << momentum_indices[0];
    for (std::size_t i = 1; i < momentum_indices.size(); ++i) {
        oss << "," << momentum_indices[i];
    }
    oss << "]";
    return oss.str();
}

std::ostream& operator<<(std::ostream& os, const InputVariable& iv) {
    return os << iv.as_string();
}

}
} // namespace Caravel