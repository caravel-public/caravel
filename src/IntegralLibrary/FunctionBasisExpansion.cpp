#include "FunctionBasisExpansion.h"

namespace Caravel {
namespace Integrals {
// ----------------------------------------------------------------------------
// ----------------------------- Instantiations -------------------------------
// ----------------------------------------------------------------------------
#define INSTANTIATE_FBE(OUT, IN)                                                                                       \
    template class FunctionBasisExpansion<FBCoefficient<Series<OUT>, IN>, StdBasisFunction<OUT, IN>>;

INSTANTIATE_FBE(C, R)
#ifdef HIGH_PRECISION
INSTANTIATE_FBE(CHP, RHP)
#endif
#ifdef VERY_HIGH_PRECISION
INSTANTIATE_FBE(CVHP, RVHP)
#endif

#ifdef USE_FINITE_FIELDS
#define INSTANTIATE_FBE_FF(CIN, COUT, OUT, IN)                                                                                \
    template class FunctionBasisExpansion<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<OUT, IN>>;

    INSTANTIATE_FBE_FF(F32, F32, C, R)
#ifdef HIGH_PRECISION
    INSTANTIATE_FBE_FF(F32, F32, CHP, RHP)
#endif
#ifdef VERY_HIGH_PRECISION
    INSTANTIATE_FBE_FF(F32, F32, CVHP, RVHP)
#endif
#endif

#define INSTANTIATE_NFBE(OUT, IN)                                                                                      \
    template class NumericFunctionBasisExpansion<Series<OUT>, StdBasisFunction<OUT, IN>>;                              \
                                                                                                                       \
    template NumericFunctionBasisExpansion<Series<OUT>, StdBasisFunction<OUT, IN>> operator+(                          \
        const NumericFunctionBasisExpansion<Series<OUT>, StdBasisFunction<OUT, IN>>& b1,                               \
        const NumericFunctionBasisExpansion<Series<OUT>, StdBasisFunction<OUT, IN>>& b2);                              \
                                                                                                                       \
    template NumericFunctionBasisExpansion<Series<OUT>, StdBasisFunction<OUT, IN>> operator-(                          \
        const NumericFunctionBasisExpansion<Series<OUT>, StdBasisFunction<OUT, IN>>& b1,                               \
        const NumericFunctionBasisExpansion<Series<OUT>, StdBasisFunction<OUT, IN>>& b2);

INSTANTIATE_NFBE(C, R)

#ifdef HIGH_PRECISION
INSTANTIATE_NFBE(CHP, RHP)
#endif

#ifdef VERY_HIGH_PRECISION
INSTANTIATE_NFBE(CVHP, RVHP)
#endif

#ifdef USE_FINITE_FIELDS
#define INSTANTIATE_NFBE_FF(COUT, OUT, IN)                                                                                      \
    template class NumericFunctionBasisExpansion<Series<COUT>, StdBasisFunction<OUT, IN>>;                              \
                                                                                                                       \
    template NumericFunctionBasisExpansion<Series<COUT>, StdBasisFunction<OUT, IN>> operator+(                          \
        const NumericFunctionBasisExpansion<Series<COUT>, StdBasisFunction<OUT, IN>>& b1,                               \
        const NumericFunctionBasisExpansion<Series<COUT>, StdBasisFunction<OUT, IN>>& b2);                              \
                                                                                                                       \
    template NumericFunctionBasisExpansion<Series<COUT>, StdBasisFunction<OUT, IN>> operator-(                          \
        const NumericFunctionBasisExpansion<Series<COUT>, StdBasisFunction<OUT, IN>>& b1,                               \
        const NumericFunctionBasisExpansion<Series<COUT>, StdBasisFunction<OUT, IN>>& b2);

INSTANTIATE_NFBE_FF(F32, C, R)

#ifdef HIGH_PRECISION
INSTANTIATE_NFBE_FF(F32, CHP, RHP)
#endif

#ifdef VERY_HIGH_PRECISION
INSTANTIATE_NFBE_FF(F32, CVHP, RVHP)
#endif
#endif

#define INSTANTIATE_OPERATOR_SCALAR(OUT, IN, TYPE)                                                                     \
    template NumericFunctionBasisExpansion<Series<OUT>, StdBasisFunction<OUT, IN>> operator*(                          \
        const TYPE& scal, const NumericFunctionBasisExpansion<Series<OUT>, StdBasisFunction<OUT, IN>>& basis);         \
                                                                                                                       \
    template NumericFunctionBasisExpansion<Series<OUT>, StdBasisFunction<OUT, IN>> operator*<>(                        \
        const NumericFunctionBasisExpansion<Series<OUT>, StdBasisFunction<OUT, IN>>& basis, const TYPE& scal);         \
                                                                                                                       \
    template NumericFunctionBasisExpansion<Series<OUT>, StdBasisFunction<OUT, IN>> operator/                           \
        <>(const NumericFunctionBasisExpansion<Series<OUT>, StdBasisFunction<OUT, IN>>& basis, const TYPE& scal);

//INSTANTIATE_OPERATOR_SCALAR(C, R, R) Problem with series operators: only allowed to multiply Series<T> with T
INSTANTIATE_OPERATOR_SCALAR(C, R, C)

#ifdef HIGH_PRECISION
//INSTANTIATE_OPERATOR_SCALAR(CHP, RHP, RHP)
#endif
#ifdef HIGH_PRECISION
INSTANTIATE_OPERATOR_SCALAR(CHP, RHP, CHP)
#endif

#ifdef VERY_HIGH_PRECISION
//INSTANTIATE_OPERATOR_SCALAR(CVHP, RVHP, RVHP)
#endif
#ifdef VERY_HIGH_PRECISION
INSTANTIATE_OPERATOR_SCALAR(CVHP, RVHP, CVHP)
#endif

#ifdef USE_FINITE_FIELDS
#define INSTANTIATE_OPERATOR_SCALAR_FF(COUT, OUT, IN, TYPE)                                                              \
    template NumericFunctionBasisExpansion<Series<COUT>, StdBasisFunction<OUT, IN>> operator*(                   \
        const TYPE& scal, const NumericFunctionBasisExpansion<Series<COUT>, StdBasisFunction<OUT, IN>>& basis);         \
                                                                                                                       \
    template NumericFunctionBasisExpansion<Series<COUT>, StdBasisFunction<OUT, IN>> operator*<>(                 \
        const NumericFunctionBasisExpansion<Series<COUT>, StdBasisFunction<OUT, IN>>& basis, const TYPE& scal);         \
                                                                                                                       \
    template NumericFunctionBasisExpansion<Series<COUT>, StdBasisFunction<OUT, IN>> operator/                    \
        <>(const NumericFunctionBasisExpansion<Series<COUT>, StdBasisFunction<OUT, IN>>& basis, const TYPE& scal);

INSTANTIATE_OPERATOR_SCALAR_FF(F32, C, R, F32)

#ifdef HIGH_PRECISION
INSTANTIATE_OPERATOR_SCALAR_FF(F32, CHP, RHP, F32)
#endif

#ifdef VERY_HIGH_PRECISION
INSTANTIATE_OPERATOR_SCALAR_FF(F32, CVHP, RVHP, F32)
#endif
#endif

} // namespace Integrals
} // namespace Caravel
