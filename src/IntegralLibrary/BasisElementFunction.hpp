#include <iterator>

namespace std {

// Specialization of the hash function
template <typename Out, typename In>
size_t hash<::Caravel::Integrals::StdBasisFunction<Out, In>>::operator()(const ::Caravel::Integrals::StdBasisFunction<Out, In>& dbf) const {
    static const hash<string> hasher;
    return hasher(dbf.as_string());
}

} // namespace std

namespace Caravel {
namespace Integrals {

// ----------------------------------------------------------------------------
// --------------------- StdBasisFunction -------------------------------------
// ----------------------------------------------------------------------------

template <typename Out, typename In>
StdBasisFunction<Out, In>::StdBasisFunction(const std::function<Out(const std::vector<In>&)>& func, const std::string& hash,
                                      const std::vector<std::string>& param_names)
    : hashstring{hash}, func{func}, parameter_names{param_names} {
    // Replace the generic argument names args[i] in the hash string by the corresponding names
    for (size_t i = 0; i < param_names.size(); ++i) {
        std::stringstream ss;
        ss << "args[" << i << "]";
        find_n_replace_all(hashstring, ss.str(), param_names[i]);
    }
    // remove "invariant_names[...]" from hash string
    for (size_t i = 0; i < param_names.size(); ++i) {
        std::stringstream ss;
        ss << ",invariant_names[" << i << "]";
        find_n_replace_all(hashstring, ss.str(), "");
    }

    // Bring the product of basis functions in the hashstring into a canonical form
    hashstring = canonical_ordering_hash_string(hashstring);

}

template <typename Out, typename In>
StdBasisFunction<Out, In>::StdBasisFunction() {
    func = [](std::vector<In>){return static_cast<Out>(1);};
    hashstring = canonical_ordering_hash_string("T(1)");
}

template <typename Out, typename In>
std::string StdBasisFunction<Out, In>::as_string() const {
    return hashstring;
}

template <typename Out, typename In>
Out StdBasisFunction<Out, In>::operator()(const std::vector<In>& args) const {
    return func(args);
}

template <typename Out, typename In>
Out StdBasisFunction<Out, In>::operator()(const std::vector<In>& args, const std::vector<std::string>& param_names) const {
    // First extract the indexes in `args` of the values needed to evaluate the function.
    auto indexes = get_indexes(param_names);
    // Construct input
    std::vector<In> input {};
    for(auto index: indexes){
        input.push_back(args[index]);
    }
    // Compute and return
    return func(input);
}


template<typename Out, typename In>
StdBasisFunction<Out, In>& StdBasisFunction<Out,In>::operator*=(const StdBasisFunction<Out,In>& other){
  if (hashstring == "T[1]"){
      *this = other;
      return *this;
  }
  else if (other.hashstring == "T[1]"){
      return *this;
  }

  hashstring = canonical_ordering_hash_string(hashstring + "*" + other.hashstring);

  std::vector<size_t> indices_of_right_function;
  size_t max_ind_of_left = parameter_names.size();

  for(const auto& it : other.parameter_names){
      auto f = std::find(parameter_names.begin(),parameter_names.end(),it);
      if(f == parameter_names.end()){
            parameter_names.push_back(it);
            indices_of_right_function.push_back(parameter_names.size()-1);
      }
      else {
          indices_of_right_function.push_back(std::distance(parameter_names.begin(),f));
      }
  }


  func = [f1=this->func,f2=other.func,indices_of_right_function,max_ind_of_left](const std::vector<In>& in) -> Out{
      std::vector<In> left(in.begin(),in.begin()+max_ind_of_left);
      std::vector<In> right;
      for(size_t i : indices_of_right_function){
          right.push_back(in[i]);
      }
      return f1(left)*f2(right);
  };

  return *this;
}

template<typename Out, typename In>
StdBasisFunction<Out, In> operator*(StdBasisFunction<Out,In> a, const StdBasisFunction<Out,In>& b){
    return (a*=b);
}

template <typename Out, typename In>
std::ostream& operator<<(std::ostream& stream, const StdBasisFunction<Out, In>& function) {
    std::string fstr = function.as_string();
    if (fstr == "T[1]") return stream << "1";
    return stream << function.as_string();
    return stream;
}

template <typename Out, typename In>
bool operator==(const StdBasisFunction<Out, In>& b1, const StdBasisFunction<Out, In>& b2) {
    return b1.as_string() == b2.as_string();
}

template <typename Out, typename In>
bool operator<(const StdBasisFunction<Out, In>& b1, const StdBasisFunction<Out, In>& b2) {
    return b1.as_string() < b2.as_string();
}

template <typename Out, typename In>
std::vector<std::size_t> StdBasisFunction<Out, In>::get_indexes(const std::vector<std::string>& names) const {
    std::vector<std::size_t> indexes {};
    for(const auto& elem: parameter_names){
        // Find
        auto it = std::find(names.begin(), names.end(), elem);
        // Throw if not found
        if(it == names.end()){ throw std::runtime_error("Could not find variable with name '"+elem+"' in the provided list!"); }
        // Save index if found
        indexes.push_back(it - names.begin());
    }
    return indexes;
}

// ----------------------------------------------------------------------------
// ------------------------- EXPLICIT INSTANTIATION ---------------------------
// ----------------------------------------------------------------------------
#define EXTERN_INSTANTIATE_BASISFUNC(OUT, IN)                                                                             \
    extern template class StdBasisFunction<OUT, IN>;                                                                      \
    extern template bool operator==(const StdBasisFunction<OUT, IN>& b1, const StdBasisFunction<OUT, IN>& b2);               \
    extern template std::ostream& operator<<(std::ostream& stream, const StdBasisFunction<OUT, IN>& basis);

EXTERN_INSTANTIATE_BASISFUNC(C, R)
#ifdef HIGH_PRECISION
EXTERN_INSTANTIATE_BASISFUNC(CHP, RHP)
#endif
#ifdef VERY_HIGH_PRECISION
EXTERN_INSTANTIATE_BASISFUNC(CVHP, RVHP)
#endif

} // namespace Integrals
} // namespace Caravel

namespace std {
#define EXTERN_INSTANTIATE_BFHASH(OUT, IN) extern template struct hash<::Caravel::Integrals::StdBasisFunction<OUT, IN>>;

EXTERN_INSTANTIATE_BFHASH(::Caravel::C, ::Caravel::R)
#ifdef HIGH_PRECISION
EXTERN_INSTANTIATE_BFHASH(::Caravel::CHP, ::Caravel::RHP)
#endif
#ifdef VERY_HIGH_PRECISION
EXTERN_INSTANTIATE_BFHASH(::Caravel::CVHP, ::Caravel::RVHP)
#endif

} // namespace std
