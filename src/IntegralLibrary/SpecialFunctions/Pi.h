#ifndef _PI_H_
#define _PI_H_

#if defined(HIGH_PRECISION) || defined(VERY_HIGH_PRECISION)
#include "qd/qd_real.h"
#endif
#include <complex>
#include "Core/typedefs.h"

namespace Caravel {
namespace Integrals {

template <typename T>
const T Pi;

template <>
inline const R Pi<R> = 3.141592653589793238;

#ifdef HIGH_PRECISION
template <>
inline const RHP Pi<RHP> = RHP("3.14159265358979323846264338327950288419716939937510582097494459");
#endif

#ifdef VERY_HIGH_PRECISION
template <>
inline const RVHP Pi<RVHP> = RVHP("3.14159265358979323846264338327950288419716939937510582097494459");
#endif

template <>
inline const C Pi<C> = C(Pi<R>, R(0.0));

#ifdef HIGH_PRECISION
template <>
inline const CHP Pi<CHP> = CHP(Pi<RHP>, RHP("0.0"));
#endif

#ifdef VERY_HIGH_PRECISION
template <>
inline const CVHP Pi<CVHP> = CVHP(Pi<RVHP>, RVHP("0.0"));
#endif

} // namespace Integrals
} // namespace Caravel

#endif
