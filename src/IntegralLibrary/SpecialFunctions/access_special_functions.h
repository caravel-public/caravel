#ifndef _ACCESS_SPECIAL_FUNCTIONS_
#define _ACCESS_SPECIAL_FUNCTIONS_


#include "Pi.h"
#include "Log.h"
#include "Power.h"
#include "Icomplex.h"
#include "InitialConditions.h"

#ifdef COMPILE_INTEGRALS
#include "Zeta.h"
#include "PolyLog.h"
#include "Goncharov.h"
#endif

#ifdef WITH_PENTAGONS
#include "PentagonFunctionsZurich.h"
#endif

#ifdef WITH_PENTAGONS_NEW
#include "PentagonFunctionsNew.h"
#endif

namespace Caravel {
namespace Integrals {

} // namespace Integrals
} // namespace Caravel

#endif // _ACCESS_SPECIAL_FUNCTIONS_
