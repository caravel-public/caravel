
namespace Caravel {
namespace Integrals {

// ----------------------------------------------------------------------------
//                          External Instantiations
// ----------------------------------------------------------------------------
#define EXTERN_INSTANTIATE_POLYLOG(OUT, IN) extern template OUT PolyLog(int, const IN&);
#define EXTERN_INSTANTIATE_L22(T) extern template T L22(const T&, const T&);

EXTERN_INSTANTIATE_POLYLOG(C, R)
#ifdef HIGH_PRECISION
EXTERN_INSTANTIATE_POLYLOG(CHP, RHP)
#endif
#ifdef VERY_HIGH_PRECISION
EXTERN_INSTANTIATE_POLYLOG(CVHP, RVHP)
#endif

EXTERN_INSTANTIATE_POLYLOG(C, C)
#ifdef HIGH_PRECISION
EXTERN_INSTANTIATE_POLYLOG(CHP, CHP)
#endif
#ifdef VERY_HIGH_PRECISION
EXTERN_INSTANTIATE_POLYLOG(CVHP, CVHP)
#endif

EXTERN_INSTANTIATE_L22(C)
#ifdef HIGH_PRECISION
EXTERN_INSTANTIATE_L22(CHP)
#endif
#ifdef VERY_HIGH_PRECISION
EXTERN_INSTANTIATE_L22(CVHP)
#endif

} // namespace Integrals
} // namespace Caravel
