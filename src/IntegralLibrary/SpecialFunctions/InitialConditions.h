#pragma once

#include <stdexcept>
#include <complex>

#include "Core/typedefs.h"

namespace Caravel {
namespace Integrals {

template <typename T>
const T divPentaBubInit = []() {
    throw std::runtime_error("divPentaBubInit not implemented for requested type!");
    return T();
}();


template <typename T>
T finBoxTriInit(const T& str5) {
    throw std::runtime_error("divPentaBubInit not implemented for requested type!");
    return T();
}

template <typename T>
T finPentaBubInit(const T& str5) {
    throw std::runtime_error("finPentaBubInit not implemented for requested type!");
    return T();
}

template <typename T>
T pentw4Init(const T& str5) {
    throw std::runtime_error("pentw4Init not implemented for requested type!");
    return T();
}

template <typename T>
const T bc49e4p4 = []() {
    throw std::runtime_error("bc49e4p4 not implemented for requested type!");
    return T();
}();

template <typename T>
const T bc50e4p4 = []() {
    throw std::runtime_error("bc50e4p4 not implemented for requested type!");
    return T();
}();

template <typename T>
const T bc60e4p4 = []() {
    throw std::runtime_error("bc60e4p4 not implemented for requested type!");
    return T();
}();

// Explicit instantiations
template <>
inline const R divPentaBubInit<R> = 8.459627416400322;

template <>
inline const C divPentaBubInit<C> = C(divPentaBubInit<R>, R(0));

template <>
inline const R bc49e4p4<R> = 36.951525880385496;

template <>
inline const C bc49e4p4<C> = C(bc49e4p4<R>, R(0));

template <>
inline const R bc50e4p4<R> = -31.607554913936756;

template <>
inline const C bc50e4p4<C> = C(bc50e4p4<R>, R(0));

template <>
inline const R bc60e4p4<R> = 17.587752547805994;

template <>
inline const C bc60e4p4<C> = C(bc60e4p4<R>, R(0));

template <> R finBoxTriInit<R>(const R& str5);
template <> C finBoxTriInit<C>(const C& str5);
template <> R finPentaBubInit<R>(const R& str5);
template <> C finPentaBubInit<C>(const C& str5);
template <> R pentw4Init<R>(const R& str5);
template <> C pentw4Init<C>(const C& str5);

} // namespace Integrals
} // namespace Caravel
