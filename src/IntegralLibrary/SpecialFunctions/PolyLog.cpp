#include "PolyLog.h"


namespace Caravel {
namespace Integrals {

// ----------------------------------------------------------------------------
//                          External Instantiations
// ----------------------------------------------------------------------------
#define INSTANTIATE_POLYLOG(OUT, IN) template OUT PolyLog(int, const IN&);
#define INSTANTIATE_L22(T) template T L22(const T&, const T&);

INSTANTIATE_POLYLOG(C, R)
#ifdef HIGH_PRECISION
INSTANTIATE_POLYLOG(CHP, RHP)
#endif
#ifdef VERY_HIGH_PRECISION
INSTANTIATE_POLYLOG(CVHP, RVHP)
#endif

INSTANTIATE_POLYLOG(C, C)
#ifdef HIGH_PRECISION
INSTANTIATE_POLYLOG(CHP, CHP)
#endif
#ifdef VERY_HIGH_PRECISION
INSTANTIATE_POLYLOG(CVHP, CVHP)
#endif

INSTANTIATE_L22(C)
#ifdef HIGH_PRECISION
INSTANTIATE_L22(CHP)
#endif
#ifdef VERY_HIGH_PRECISION
INSTANTIATE_L22(CVHP)
#endif

} // namespace Integrals
} // namespace Caravel
