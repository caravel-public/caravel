#include "Core/Utilities.h"
#include "misc/DeltaZero.h"
#include "PentagonFunctionsZurich.h"
#include "PentagonFunctionsZurich.hpp"
#include "access_special_functions.h"
#include "Core/settings.h"

#include <algorithm>
#include <sstream>
#include <stdexcept>

namespace Caravel {
namespace Integrals {

using settings::integrals::pentagon_functions_integration_precision;
using settings::integrals::pentagon_functions_max_subintervals;

// ---------------------------------------------------------------------------
//      SPECIALISATIONS FOR DOUBLE AND COMPLEX<DOUBLE>
// ---------------------------------------------------------------------------

// real input
template <>
std::complex<double> pentagon_f_1_1(const double& v1) {
    static const double dummy(0);
    return evaluatelower(1, v1, dummy, dummy, dummy, dummy);
}

// complex input
template <>
std::complex<double> pentagon_f_1_1(const std::complex<double>& v1) {
    return pentagon_f_1_1<std::complex<double>, double>(v1.real());
}

// real input
template <>
std::complex<double> pentagon_f_2_1(const double& v1, const double& v3) {
    static const double dummy(0);
    return evaluatelower(6, v1, dummy, v3, dummy, dummy);
}

// complex input
template <>
std::complex<double> pentagon_f_2_1(const std::complex<double>& v1, const std::complex<double>& v3) {
    return pentagon_f_2_1<std::complex<double>, double>(v1.real(), v3.real());
}

// real input
template <>
std::complex<double> pentagon_f_3_1(const double& v1, const double& v3) {
    static const double dummy(0);
    return evaluatelower(11, v1, dummy, v3, dummy, dummy);
}

// complex input
template <>
std::complex<double> pentagon_f_3_1(const std::complex<double>& v1, const std::complex<double>& v3) {
    return pentagon_f_3_1<std::complex<double>, double>(v1.real(), v3.real());
}

// real input
template <>
std::complex<double> pentagon_f_3_2(const double& v1, const double& v3) {
    static const double dummy(0);
    return evaluatelower(16, v1, dummy, v3, dummy, dummy);
}

// complex input
template <>
std::complex<double> pentagon_f_3_2(const std::complex<double>& v1, const std::complex<double>& v3) {
    return pentagon_f_3_2<std::complex<double>, double>(v1.real(), v3.real());
}

// real input
template <>
std::complex<double> pentagon_f_3_3(const double& v1, const double& v2, const double& v4) {
    static const double dummy(0);
    return evaluatelower(21, v1, v2, dummy, v4, dummy);
}

// complex input
template <>
std::complex<double> pentagon_f_3_3(const std::complex<double>& v1, const std::complex<double>& v2,
                                    const std::complex<double>& v4) {
    return pentagon_f_3_3<std::complex<double>, double>(v1.real(), v2.real(), v4.real());
}

// real input
template <>
std::complex<double> pentagon_f_3_4(const double& v1, const double& v2, const double& v3, const double& v4,
                                    const double& v5, const double& str5) {
    return str5 * evaluatelower(26, v1, v2, v3, v4, v5);
}

// complex input
template <>
std::complex<double> pentagon_f_3_4(const std::complex<double>& v1, const std::complex<double>& v2,
                                    const std::complex<double>& v3, const std::complex<double>& v4,
                                    const std::complex<double>& v5, const std::complex<double>& str5) {
    return pentagon_f_3_4<std::complex<double>, double>(v1.real(), v2.real(), v3.real(), v4.real(), v5.real(), str5.real());
}

void special_f_4_two_vars_choose_region(size_t i,std::vector<double>& dummy, const double& v1, const double& v3, const std::string& s1, const std::string& s3){
    const auto& initial = PhysRegSelector::get_initial();
    // take care of Euclidean (with sanity check)
    if((s1=="EUCLIDEAN" && s3=="EUCLIDEAN") || initial.size()==0){
        if(v1>0. || v3>0.){
            std::cerr<<"ERROR: inferred Euclidean region, but called pentagon_f_4_"<<i<<"("<<v1<<", "<<v3<<", "<<s1<<", "<<s3<<") !"<<std::endl;
            throw std::runtime_error("Bad call to pentagon_f_4_"+std::to_string(i));
        }
        // set all to negative
        dummy = {-1., -2.1, -3.7};
        return;
    }
    // simple case
    if(v1<0. && v3>0.){
        // a sign pick
        dummy = {1., -2.1, 3.7};
        return;
    }
    // simple case
    if(v1>0. && v3<0.){
        // a sign pick
        dummy = {1., 2.1, -3.7};
        return;
    }
    // Euclidean is covered
    if(v1<0. && v3<0.){
        // signs to pick an alternating case (like {1,3}->{2,4,5})
        dummy = {-1., -2.1, 3.7};
        return;
    }
    // we are left with a 'split' configuration like {2,3} -> {1,4,5}
    if(v1>0. && v3>0.){
        std::string initial_sij = "s";
        for(auto& ii:initial)  initial_sij+=std::to_string(ii);
        if(s1==initial_sij){
            dummy = {-1., 2.1, -3.7};
            return;
        }
        else if(s3==initial_sij){
            dummy = {-1., -2.1, 3.7};
            return;
        }
    }
    std::cerr<<"ERROR: couldn't infer region in call pentagon_f_4_"<<i<<"("<<v1<<", "<<v3<<", "<<s1<<", "<<s3<<") ! initial: "<<initial<<std::endl;
    throw std::runtime_error("Could not find proper region for pentagon_f_4_"+std::to_string(i));
}

// real input
template <>
std::complex<double> pentagon_f_4_1(const double& v1, const double& v3, const std::string& s1, const std::string& s3) {
    std::vector<double> dummy(3,0);
    special_f_4_two_vars_choose_region(1,dummy,v1,v3,s1,s3);
    return integrate(1, v1, dummy[0], v3, dummy[1], dummy[2], pentagon_functions_integration_precision, pentagon_functions_max_subintervals);
}

// complex input
template <>
std::complex<double> pentagon_f_4_1(const std::complex<double>& v1, const std::complex<double>& v3, const std::string& s1, const std::string& s3) {
    return pentagon_f_4_1<std::complex<double>, double>(v1.real(), v3.real(), s1, s3);
}

// real input
template <>
std::complex<double> pentagon_f_4_2(const double& v1, const double& v3, const std::string& s1, const std::string& s3) {
    std::vector<double> dummy(3,0);
    special_f_4_two_vars_choose_region(2,dummy,v1,v3,s1,s3);
    return integrate(6, v1, dummy[0], v3, dummy[1], dummy[2], pentagon_functions_integration_precision, pentagon_functions_max_subintervals);
}

// complex input
template <>
std::complex<double> pentagon_f_4_2(const std::complex<double>& v1, const std::complex<double>& v3, const std::string& s1, const std::string& s3) {
    return pentagon_f_4_2<std::complex<double>, double>(v1.real(), v3.real(), s1, s3);
}

// real input
template <>
std::complex<double> pentagon_f_4_3(const double& v1, const double& v3, const std::string& s1, const std::string& s3) {
    std::vector<double> dummy(3,0);
    special_f_4_two_vars_choose_region(3,dummy,v1,v3,s1,s3);
    return integrate(11, v1, dummy[0], v3, dummy[1], dummy[2], pentagon_functions_integration_precision, pentagon_functions_max_subintervals);
}

// complex input
template <>
std::complex<double> pentagon_f_4_3(const std::complex<double>& v1, const std::complex<double>& v3, const std::string& s1, const std::string& s3) {
    return pentagon_f_4_3<std::complex<double>, double>(v1.real(), v3.real(), s1, s3);
}

void special_f_4_three_vars_choose_region(size_t i,std::vector<double>& dummy, const double& v1, const double& v2,  const double& v4){
    const auto& initial = PhysRegSelector::get_initial();
    // 'default' when n==0
    const auto n = PhysRegSelector::get_n();
    // take care of Euclidean (with sanity check)
    if(n==0 || initial.size()==0){
        if(v1>0. || v2>0. || v4>0.){
            std::cerr<<"ERROR: inferred Euclidean region, but called pentagon_f_4_"<<i<<"("<<v1<<", "<<v2<<", "<<v4<<") !"<<std::endl;
            throw std::runtime_error("Bad call to pentagon_f_4_"+std::to_string(i));
        }
        // set all to negative
        dummy = {-1., -2.1};
        return;
    }
    // Euclidean is covered
    if(v1<0. && v2<0. && v4<0.){
        // signs to pick an alternating case (like {2,5}->{1,3,4})
        dummy = {-1., 3.7};
        return;
    }
    // simple case
    if(v1<0. && v2<0. && v4>0.){
        dummy = {-1., -2.1};
        return;
    }
    // simple case
    if(v1<0. && v2>0. && v4<0.){
        dummy = {1., 2.1};
        return;
    }
    // simple case
    if(v1>0. && v2<0. && v4<0.){
        dummy = {1., 2.1};
        return;
    }
    // simple case
    if(v1>0. && v2>0. && v4>0.){
        dummy = {-1., -2.1};
        return;
    }
    // simple case
    if(v1<0. && v2>0. && v4>0.){
        dummy = {-1., 2.1};
        return;
    }
    // simple case
    if(v1>0. && v2<0. && v4>0.){
        dummy = {1., -2.1};
        return;
    }
    std::cerr<<"ERROR: couldn't infer region in call pentagon_f_4_"<<i<<"("<<v1<<", "<<v2<<", "<<v4<<") ! initial: "<<initial<<std::endl;
    throw std::runtime_error("Could not find proper region for pentagon_f_4_"+std::to_string(i));
}

// real input
template <>
std::complex<double> pentagon_f_4_4(const double& v1, const double& v2, const double& v4) {
    std::vector<double> dummy(2,0);
    special_f_4_three_vars_choose_region(4,dummy,v1,v2,v4);
    return integrate(16, v1, v2, dummy[0], v4, dummy[1], pentagon_functions_integration_precision, pentagon_functions_max_subintervals);
}

// complex input
template <>
std::complex<double> pentagon_f_4_4(const std::complex<double>& v1, const std::complex<double>& v2,
                                    const std::complex<double>& v4) {
    return pentagon_f_4_4<std::complex<double>, double>(v1.real(), v2.real(), v4.real());
}

// real input
template <>
std::complex<double> pentagon_f_4_5(const double& v1, const double& v2, const double& v4) {
    std::vector<double> dummy(2,0);
    special_f_4_three_vars_choose_region(5,dummy,v1,v2,v4);
    return integrate(21, v1, v2, dummy[0], v4, dummy[1], pentagon_functions_integration_precision, pentagon_functions_max_subintervals);
}

// complex input
template <>
std::complex<double> pentagon_f_4_5(const std::complex<double>& v1, const std::complex<double>& v2,
                                    const std::complex<double>& v4) {
    return pentagon_f_4_5<std::complex<double>, double>(v1.real(), v2.real(), v4.real());
}

// real input
template <>
std::complex<double> pentagon_f_4_6(const double& v1, const double& v3, const double& v5) {
    std::vector<double> dummy(2,0);
    special_f_4_three_vars_choose_region(6,dummy,v5,v1,v3);
    return integrate(26, v1, dummy[0], v3, dummy[1], v5, pentagon_functions_integration_precision, pentagon_functions_max_subintervals);
}

// complex input
template <>
std::complex<double> pentagon_f_4_6(const std::complex<double>& v1, const std::complex<double>& v3,
                                    const std::complex<double>& v5) {
    return pentagon_f_4_6<std::complex<double>, double>(v1.real(), v3.real(), v5.real());
}

// real input
template <>
std::complex<double> pentagon_f_4_7(const double& v1, const double& v3, const double& v4) {
    std::vector<double> dummy(2,0);
    special_f_4_three_vars_choose_region(7,dummy,v3,v4,v1);
    return integrate(31, v1, dummy[1], v3, v4, dummy[0], pentagon_functions_integration_precision, pentagon_functions_max_subintervals);
}

// complex input
template <>
std::complex<double> pentagon_f_4_7(const std::complex<double>& v1, const std::complex<double>& v3,
                                    const std::complex<double>& v4) {
    return pentagon_f_4_7<std::complex<double>, double>(v1.real(), v3.real(), v4.real());
}

// real input
template <>
std::complex<double> pentagon_f_4_8(const double& v2, const double& v4, const double& v5) {
    std::vector<double> dummy(2,0);
    special_f_4_three_vars_choose_region(8,dummy,v4,v5,v2);
    return integrate(36, dummy[0], v2, dummy[1], v4, v5, pentagon_functions_integration_precision, pentagon_functions_max_subintervals);
}

// complex input
template <>
std::complex<double> pentagon_f_4_8(const std::complex<double>& v2, const std::complex<double>& v4,
                                    const std::complex<double>& v5) {
    return pentagon_f_4_8<std::complex<double>, double>(v2.real(), v4.real(), v5.real());
}

// real input
template <>
std::complex<double> pentagon_f_4_9(const double& v1, const double& v3, const double& v4) {
    std::vector<double> dummy(2,0);
    special_f_4_three_vars_choose_region(9,dummy,v3,v4,v1);
    return integrate(41, v1, dummy[1], v3, v4, dummy[0], pentagon_functions_integration_precision, pentagon_functions_max_subintervals);
}

// complex input
template <>
std::complex<double> pentagon_f_4_9(const std::complex<double>& v1, const std::complex<double>& v3,
                                    const std::complex<double>& v4) {
    return pentagon_f_4_9<std::complex<double>, double>(v1.real(), v3.real(), v4.real());
}

// real input
template <>
std::complex<double> pentagon_f_4_10(const double& v1, const double& v2, const double& v3, const double& v4,
                                     const double& v5, const double& str5) {
    return str5 * integrate(46, v1, v2, v3, v4, v5, pentagon_functions_integration_precision, pentagon_functions_max_subintervals);
}

// complex input
template <>
std::complex<double> pentagon_f_4_10(const std::complex<double>& v1, const std::complex<double>& v2,
                                     const std::complex<double>& v3, const std::complex<double>& v4,
                                     const std::complex<double>& v5, const std::complex<double>& str5) {
    return pentagon_f_4_10<std::complex<double>, double>(v1.real(), v2.real(), v3.real(), v4.real(), v5.real(), str5.real());
}

// real input
template <>
std::complex<double> pentagon_f_4_11(const double& v1, const double& v2, const double& v3, const double& v4,
                                     const double& v5) {
    return integrate(51, v1, v2, v3, v4, v5, pentagon_functions_integration_precision, pentagon_functions_max_subintervals);
}

// complex input
template <>
std::complex<double> pentagon_f_4_11(const std::complex<double>& v1, const std::complex<double>& v2,
                                     const std::complex<double>& v3, const std::complex<double>& v4,
                                     const std::complex<double>& v5) {
    return pentagon_f_4_11<std::complex<double>, double>(v1.real(), v2.real(), v3.real(), v4.real(), v5.real());
}

// real input
template <>
std::complex<double> pentagon_f_4_12(const double& v1, const double& v2, const double& v3, const double& v4,
                                     const double& v5, const double& str5) {
    return str5 * integrate(56, v1, v2, v3, v4, v5, pentagon_functions_integration_precision, pentagon_functions_max_subintervals);
}

// complex input
template <>
std::complex<double> pentagon_f_4_12(const std::complex<double>& v1, const std::complex<double>& v2,
                                     const std::complex<double>& v3, const std::complex<double>& v4,
                                     const std::complex<double>& v5, const std::complex<double>& str5) {
    return pentagon_f_4_12<std::complex<double>, double>(v1.real(), v2.real(), v3.real(), v4.real(), v5.real(), str5.real());
}



} // namespace Integrals
} // namespace Caravel
