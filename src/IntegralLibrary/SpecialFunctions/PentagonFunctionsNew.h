#pragma once

#include <vector>
#include <complex>
#include <iostream>
#include <stdexcept>
#include <string>

#include "Core/PhysRegSelector.h"

namespace Caravel {
/// Contains everything related to the Masters.
namespace Integrals {
template <typename Out, typename In>
Out MI(const std::string name, const int index, const int epPower, const In& v1){return Out(0);}
template <typename Out, typename In>
Out MI(const std::string name, const int index, const int epPower, const In& v1, const In& v2){return Out(0);}
template <typename Out, typename In>
Out MI(const std::string name, const int index, const int epPower, const In& v1, const In& v2, const In& v3){return Out(0);}
template <typename Out, typename In>
Out MI(const std::string name, const int index, const int epPower, const In& v1, const In& v2, const In& v3, const In& v4){return Out(0);}
template <typename Out, typename In>
Out MI(const std::string name, const int index, const int epPower, const In& v1, const In& v2, const In& v3, const In& v4, const In& v5){return Out(0);}
template <typename Out, typename In>
Out MI(const std::string name, const int index, const int epPower, const In& v1, const In& v2, const In& v3, const In& v4, const In& v5, const In& v6){return Out(0);}

} // namespace Integrals
} // namespace Caravel

// Implementations
//#include "PentagonFunctionsNew.hpp"
