#include "Goncharov.h"


namespace Caravel {
namespace Integrals {

/// For double precision set the number of digits in GiNaC to 18.
template <>
void set_digits<R>() {
    GiNaC::Digits = 18;
}

/// For double precision set the number of digits in GiNaC to 18.
template <>
void set_digits<C>() {
    GiNaC::Digits = 18;
}

/// For double double precision set the number of digits in GiNaC to 34.
#ifdef HIGH_PRECISION
template <>
void set_digits<RHP>() {
    GiNaC::Digits = 34;
}
#endif

/// For double double precision set the number of digits in GiNaC to 34.
#ifdef HIGH_PRECISION
template <>
void set_digits<CHP>() {
    GiNaC::Digits = 34;
}
#endif

/// For quadruple double set the number of digits in GiNaC to 66.
#ifdef VERY_HIGH_PRECISION
template <>
void set_digits<RVHP>() {
    GiNaC::Digits = 66;
}
#endif

/// For quadruple double set the number of digits in GiNaC to 66.
#ifdef VERY_HIGH_PRECISION
template <>
void set_digits<CVHP>() {
    GiNaC::Digits = 66;
}
#endif
/// Check whether a number is considered to be zero.
///
/// A double number is considered zero if its absolute value is smaller than
/// 1e-13.
template <>
bool is_zero<R>(const R& in) {
    return (myabs(in) < 1e-13) ? true : false;
}

/// Check whether a number is considered to be zero.
///
/// A double number is considered zero if its absolute value is smaller than
/// 1e-13.
template <>
bool is_zero<C>(const C& in) {
    return (myabs(in.real()) < 1e-13 && myabs(in.imag()) < 1e-13) ? true : false;
}

/// Check whether a number is considered to be zero.
///
/// A double double number is considered zero if its absolute value is smaller
/// than 1e-29.
#ifdef HIGH_PRECISION
template <>
bool is_zero<RHP>(const RHP& in) {
    return (myabs(in) < RHP("1e-29")) ? true : false;
}
#endif

/// Check whether a number is considered to be zero.
///
/// A double double number is considered zero if its absolute value is smaller
/// than 1e-29.
#ifdef HIGH_PRECISION
template <>
bool is_zero<CHP>(const CHP& in) {
    return (myabs(in.real()) < RHP("1e-29") && myabs(in.imag()) < RHP("1e-29")) ? true : false;
}
#endif

/// Check whether a number is considered to be zero.
///
/// A quadruple double number is considered to be zero if its absolute value is
/// smaller than 1e-61
#ifdef VERY_HIGH_PRECISION
template <>
bool is_zero<RVHP>(const RVHP& in) {
    return (myabs(in) < RVHP("1e-61")) ? true : false;
}
#endif

/// Check whether a number is considered to be zero.
///
/// A quadruple double number is considered to be zero if its absolute value is
/// smaller than 1e-61
#ifdef VERY_HIGH_PRECISION
template <>
bool is_zero<CVHP>(const CVHP& in) {
    return (myabs(in.real()) < RVHP("1e-61") && myabs(in.imag()) < RVHP("1e-61")) ? true : false;
}
#endif

// ----------------------------------------------------------------------------
//                              Instantiations
// ----------------------------------------------------------------------------
#define INSTANTIATE_GONCHAROV(OUT, IN)                                                                          \
    template OUT G(const IN&, const IN&, const IN&, const IN&, const IN&, const IN&, const IN&);                \
    template OUT G(const IN&, const IN&, const IN&, const IN&, const IN&, const IN&);                           \
    template OUT G(const IN&, const IN&, const IN&, const IN&, const IN&);                                      \
    template OUT G(const IN&, const IN&, const IN&, const IN&);                                                 \
    template OUT G(const IN&, const IN&, const IN&);                                                            \
    template OUT G(const IN&, const IN&);

INSTANTIATE_GONCHAROV(C, R)
#ifdef HIGH_PRECISION
INSTANTIATE_GONCHAROV(CHP, RHP)
#endif
#ifdef VERY_HIGH_PRECISION
INSTANTIATE_GONCHAROV(CVHP, RVHP)
#endif

INSTANTIATE_GONCHAROV(C, C)
#ifdef HIGH_PRECISION
INSTANTIATE_GONCHAROV(CHP, CHP)
#endif
#ifdef VERY_HIGH_PRECISION
INSTANTIATE_GONCHAROV(CVHP, CVHP)
#endif

} // namespace Integrals
} // namespace Caravel
