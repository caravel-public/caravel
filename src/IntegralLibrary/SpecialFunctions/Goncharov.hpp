
namespace Caravel {
namespace Integrals {

// ----------------------------------------------------------------------------
//                          External Instantiations
// ----------------------------------------------------------------------------
#define EXTERN_INSTANTIATE_GONCHAROV(OUT, IN)                                                                          \
    extern template OUT G(const IN&, const IN&, const IN&, const IN&, const IN&, const IN&, const IN&);                \
    extern template OUT G(const IN&, const IN&, const IN&, const IN&, const IN&, const IN&);                           \
    extern template OUT G(const IN&, const IN&, const IN&, const IN&, const IN&);                                      \
    extern template OUT G(const IN&, const IN&, const IN&, const IN&);                                                 \
    extern template OUT G(const IN&, const IN&, const IN&);                                                            \
    extern template OUT G(const IN&, const IN&);

EXTERN_INSTANTIATE_GONCHAROV(C, R)
#ifdef HIGH_PRECISION
EXTERN_INSTANTIATE_GONCHAROV(CHP, RHP)
#endif
#ifdef VERY_HIGH_PRECISION
EXTERN_INSTANTIATE_GONCHAROV(CVHP, RVHP)
#endif

EXTERN_INSTANTIATE_GONCHAROV(C, C)
#ifdef HIGH_PRECISION
EXTERN_INSTANTIATE_GONCHAROV(CHP, CHP)
#endif
#ifdef VERY_HIGH_PRECISION
EXTERN_INSTANTIATE_GONCHAROV(CVHP, CVHP)
#endif

} // namespace Integrals
} // namespace Caravel
