#include "Zeta.h"


namespace Caravel {
namespace Integrals {

/// \f$ \zeta_2 \f$ for type R.
template <>
const R _zeta2<R> = 1.6449340668482264364724151666460251892189499012067984377355582294;

/// \f$ \zeta_2 \f$ for type RHP.
#ifdef HIGH_PRECISION
template <>
const RHP _zeta2<RHP> = RHP("1.6449340668482264364724151666460251892189499012067984377355582294");
#endif

/// \f$ \zeta_2 \f$ for type RVHP.
#ifdef VERY_HIGH_PRECISION
template <>
const RVHP _zeta2<RVHP> = RVHP("1.6449340668482264364724151666460251892189499012067984377355582294");
#endif

/// \f$ \zeta_2 \f$ for type C.
template <>
const C _zeta2<C> = C(_zeta2<R>, R(0));

/// \f$ \zeta_2 \f$ for type CHP.
#ifdef HIGH_PRECISION
template <>
const CHP _zeta2<CHP> = CHP(_zeta2<RHP>, RHP("0"));
#endif

/// \f$ \zeta_2 \f$ for type CVHP.
#ifdef VERY_HIGH_PRECISION
template <>
const CVHP _zeta2<CVHP> = CVHP(_zeta2<RVHP>, RVHP("0"));
#endif

/// \f$ \zeta_3 \f$ for type R.
template <>
const R _zeta3<R> = 1.2020569031595942853997381615114499907649862923404988817922715553;

/// \f$ \zeta_3 \f$ for type RHP.
#ifdef HIGH_PRECISION
template <>
const RHP _zeta3<RHP> = RHP("1.2020569031595942853997381615114499907649862923404988817922715553");
#endif

/// \f$ \zeta_3 \f$ for type RVHP.
#ifdef VERY_HIGH_PRECISION
template <>
const RVHP _zeta3<RVHP> = RVHP("1.2020569031595942853997381615114499907649862923404988817922715553");
#endif

/// \f$ \zeta_3 \f$ for type C.
template <>
const C _zeta3<C> = C(_zeta3<R>, R(0));

/// \f$ \zeta_3 \f$ for type CHP.
#ifdef HIGH_PRECISION
template <>
const CHP _zeta3<CHP> = CHP(_zeta3<RHP>, RHP("0"));
#endif

/// \f$ \zeta_3 \f$ for type CVHP.
#ifdef VERY_HIGH_PRECISION
template <>
const CVHP _zeta3<CVHP> = CVHP(_zeta3<RVHP>, RVHP("0"));
#endif


/// \f$ \zeta_5 \f$ for type R.
template <>
const R _zeta5<R> = 1.036927755143369926331365486457034168057080919501912811974192678;

/// \f$ \zeta_5 \f$ for type RHP.
#ifdef HIGH_PRECISION
template <>
const RHP _zeta5<RHP> = RHP("1.036927755143369926331365486457034168057080919501912811974192678");
#endif

/// \f$ \zeta_5 \f$ for type RVHP.
#ifdef VERY_HIGH_PRECISION
template <>
const RVHP _zeta5<RVHP> = RVHP("1.036927755143369926331365486457034168057080919501912811974192678");
#endif

/// \f$ \zeta_5 \f$ for type C.
template <>
const C _zeta5<C> = C(_zeta5<R>, R(0));

/// \f$ \zeta_5 \f$ for type CHP.
#ifdef HIGH_PRECISION
template <>
const CHP _zeta5<CHP> = CHP(_zeta5<RHP>, RHP("0"));
#endif

/// \f$ \zeta_5 \f$ for type CVHP.
#ifdef VERY_HIGH_PRECISION
template <>
const CVHP _zeta5<CVHP> = CVHP(_zeta5<RVHP>, RVHP("0"));
#endif

// ----------------------------------------------------------------------------
//                        Instantiations
// ----------------------------------------------------------------------------
#define INSTANTIATE_ZETA(T) template T zeta(int);

INSTANTIATE_ZETA(C)
#ifdef HIGH_PRECISION
INSTANTIATE_ZETA(CHP)
#endif
#ifdef VERY_HIGH_PRECISION
INSTANTIATE_ZETA(CVHP)
#endif

} // namespace Integrals
} // namespace Caravel
