
namespace Caravel {
namespace Integrals {

// ----------------------------------------------------------------------------
//                          External Instantiations
// ----------------------------------------------------------------------------
#define EXTERN_INSTANTIATE_LOG(T) extern template T Log(const T&);

EXTERN_INSTANTIATE_LOG(R)
#ifdef HIGH_PRECISION
#ifdef HIGH_PRECISION
EXTERN_INSTANTIATE_LOG(RHP)
#endif
#endif
#ifdef VERY_HIGH_PRECISION
#ifdef VERY_HIGH_PRECISION
EXTERN_INSTANTIATE_LOG(RVHP)
#endif
#endif

EXTERN_INSTANTIATE_LOG(C)
#ifdef HIGH_PRECISION
#ifdef HIGH_PRECISION
EXTERN_INSTANTIATE_LOG(CHP)
#endif
#endif
#ifdef VERY_HIGH_PRECISION
#ifdef VERY_HIGH_PRECISION
EXTERN_INSTANTIATE_LOG(CVHP)
#endif
#endif

} // namespace Integrals
} // namespace Caravel
