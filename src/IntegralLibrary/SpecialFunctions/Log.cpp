#include "Log.h"


namespace Caravel {
namespace Integrals {

// ----------------------------------------------------------------------------
//                              Instantiations
// ----------------------------------------------------------------------------
#define INSTANTIATE_LOG(T) template T Log(const T&);

INSTANTIATE_LOG(R)
#ifdef HIGH_PRECISION
#ifdef HIGH_PRECISION
INSTANTIATE_LOG(RHP)
#endif
#endif
#ifdef VERY_HIGH_PRECISION
#ifdef VERY_HIGH_PRECISION
INSTANTIATE_LOG(RVHP)
#endif
#endif

INSTANTIATE_LOG(C)
#ifdef HIGH_PRECISION
#ifdef HIGH_PRECISION
INSTANTIATE_LOG(CHP)
#endif
#endif
#ifdef VERY_HIGH_PRECISION
#ifdef VERY_HIGH_PRECISION
INSTANTIATE_LOG(CVHP)
#endif
#endif

} // namespace Integrals
} // namespace Caravel
