#include <type_traits>

namespace Caravel {
namespace Integrals {

// ----------------------------------------------------------------------------
// Code generation.
//
// I know macros directly lead to hell but in this case, I chose to use them
// because I have to generate a lot of code that only differs by the indices
// in the function names and sometimes an additional function index. So it
// is easiest to use macros to generate this code to avoid copying around
// a lot of code and keep the code that is to be maintained in a single spot.
//
// ----------------------------------------------------------------------------

// Construct the name of the function which will be of the form
// <base><connect><i><connect><j>.
#define FUNCTION_NAME(base, i, j, connect) base##connect##i##connect##j

// Providing a default implementation that fails at compile time is the same for every function, so we
// use a macro that does it for every function by just providing the function indices.
#define DEFAULT_IMPLEMENTATION_1(i, j)                                                                                 \
    template <typename Out, typename In>                                                                               \
    Out FUNCTION_NAME(pentagon_f, i, j, _)(const In& x) {                                                              \
        static_assert(std::is_same<Out, std::complex<double>>::value &&                                                \
                          (std::is_same<In, double>::value || std::is_same<In, std::complex<double>>::value),          \
                      "pentagon functions only implemented for double precision!");                                    \
        Out toret{};                                                                                                   \
        return toret;                                                                                                  \
    }

#define DEFAULT_IMPLEMENTATION_2(i, j)                                                                                 \
    template <typename Out, typename In>                                                                               \
    Out FUNCTION_NAME(pentagon_f, i, j, _)(const In& x, const In& y) {                                                 \
        static_assert(std::is_same<Out, std::complex<double>>::value &&                                                \
                          (std::is_same<In, double>::value || std::is_same<In, std::complex<double>>::value),          \
                      "pentagon functions only implemented for double precision!");                                    \
        Out toret{};                                                                                                   \
        return toret;                                                                                                  \
    }

#define DEFAULT_IMPLEMENTATION_3(i, j)                                                                                 \
    template <typename Out, typename In>                                                                               \
    Out FUNCTION_NAME(pentagon_f, i, j, _)(const In& x, const In& y, const In& z) {                                    \
        static_assert(std::is_same<Out, std::complex<double>>::value &&                                                \
                          (std::is_same<In, double>::value || std::is_same<In, std::complex<double>>::value),          \
                      "pentagon functions only implemented for double precision!");                                    \
        Out toret{};                                                                                                   \
        return toret;                                                                                                  \
    }

#define DEFAULT_IMPLEMENTATION_5(i, j)                                                                                 \
    template <typename Out, typename In>                                                                               \
    Out FUNCTION_NAME(pentagon_f, i, j, _)(const In& v1, const In& v2, const In& v3, const In& v4, const In& v5) {     \
        static_assert(std::is_same<Out, std::complex<double>>::value &&                                                \
                          (std::is_same<In, double>::value || std::is_same<In, std::complex<double>>::value),          \
                      "pentagon functions only implemented for double precision!");                                    \
        Out toret{};                                                                                                   \
        return toret;                                                                                                  \
    }

#define DEFAULT_IMPLEMENTATION_6(i, j)                                                                                 \
    template <typename Out, typename In>                                                                               \
    Out FUNCTION_NAME(pentagon_f, i, j, _)(const In& v1, const In& v2, const In& v3, const In& v4, const In& v5, const In& str5) {     \
        static_assert(std::is_same<Out, std::complex<double>>::value &&                                                \
                          (std::is_same<In, double>::value || std::is_same<In, std::complex<double>>::value),          \
                      "pentagon functions only implemented for double precision!");                                    \
        Out toret{};                                                                                                   \
        return toret;                                                                                                  \
    }

#define DEFAULT_IMPLEMENTATION_WITH_NAMES_2(i, j)                                                                      \
    template <typename Out, typename In>                                                                               \
    Out FUNCTION_NAME(pentagon_f, i, j, _)(const In& x, const In& y, const std::string&, const std::string&) { \
        static_assert(std::is_same<Out, std::complex<double>>::value &&                                                \
                          (std::is_same<In, double>::value || std::is_same<In, std::complex<double>>::value),          \
                      "pentagon functions only implemented for double precision!");                                    \
        Out toret{};                                                                                                   \
        return toret;                                                                                                  \
    }

DEFAULT_IMPLEMENTATION_1(1, 1)
DEFAULT_IMPLEMENTATION_2(2, 1)
DEFAULT_IMPLEMENTATION_2(3, 1)
DEFAULT_IMPLEMENTATION_2(3, 2)
DEFAULT_IMPLEMENTATION_3(3, 3)
DEFAULT_IMPLEMENTATION_6(3, 4)
DEFAULT_IMPLEMENTATION_WITH_NAMES_2(4, 1)
DEFAULT_IMPLEMENTATION_WITH_NAMES_2(4, 2)
DEFAULT_IMPLEMENTATION_WITH_NAMES_2(4, 3)
DEFAULT_IMPLEMENTATION_3(4, 4)
DEFAULT_IMPLEMENTATION_3(4, 5)
DEFAULT_IMPLEMENTATION_3(4, 6)
DEFAULT_IMPLEMENTATION_3(4, 7)
DEFAULT_IMPLEMENTATION_3(4, 8)
DEFAULT_IMPLEMENTATION_3(4, 9)
DEFAULT_IMPLEMENTATION_6(4, 10)
DEFAULT_IMPLEMENTATION_5(4, 11)
DEFAULT_IMPLEMENTATION_6(4, 12)

} // namespace Integrals
} // namespace Caravel
