
namespace Caravel {
namespace Integrals {

// ----------------------------------------------------------------------------
//                          External Instantiations
// ----------------------------------------------------------------------------
#define EXTERN_INSTANTIATE_ZETA(T) extern template T zeta(int);

EXTERN_INSTANTIATE_ZETA(C)
#ifdef HIGH_PRECISION
EXTERN_INSTANTIATE_ZETA(CHP)
#endif
#ifdef VERY_HIGH_PRECISION
EXTERN_INSTANTIATE_ZETA(CVHP)
#endif

} // namespace Integrals
} // namespace Caravel
