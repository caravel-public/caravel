#ifndef _LOG_H_
#define _LOG_H_

#include <cmath>
using std::log;

#if defined(HIGH_PRECISION) || defined(VERY_HIGH_PRECISION)
#include "Core/qd_suppl.h"
#endif
#include "Core/f_suppl.h"
#include "Core/typedefs.h"

namespace Caravel {
namespace Integrals {

template <typename T>
T Log(const T& arg){
    return log(arg);
}

} // namespace Integrals
} // namespace Caravel

// External Instantiations
#include "Log.hpp"

#endif // _LOG_H_
