#ifndef _ICOMPLEX_H_
#define _ICOMPLEX_H_

#include "Core/typedefs.h"

namespace Caravel {
namespace Integrals {

template <typename T>
const T icomplex;

} // namespace Integrals
} // namespace Caravel

#endif
