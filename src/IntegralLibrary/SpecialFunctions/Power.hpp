
namespace Caravel {
namespace Integrals {

// ----------------------------------------------------------------------------
//                          External Instantiations
// ----------------------------------------------------------------------------
#define EXTERN_INSTANTIATE_POWER(T) extern template T prodpow(const T&, std::size_t);

EXTERN_INSTANTIATE_POWER(R)
#ifdef HIGH_PRECISION
EXTERN_INSTANTIATE_POWER(RHP)
#endif
#ifdef VERY_HIGH_PRECISION
EXTERN_INSTANTIATE_POWER(RVHP)
#endif

EXTERN_INSTANTIATE_POWER(C)
#ifdef HIGH_PRECISION
EXTERN_INSTANTIATE_POWER(CHP)
#endif
#ifdef VERY_HIGH_PRECISION
EXTERN_INSTANTIATE_POWER(CVHP)
#endif

} // namespace Integrals
} // namespace Caravel
