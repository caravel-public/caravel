#ifndef _POWER_H_
#define _POWER_H_

#include <cmath>

#include "Core/typedefs.h"

namespace Caravel {
namespace Integrals {

template <typename T, typename Exp>
T Power(const T& arg, Exp power){
    using std::pow;

    return pow(arg, power);
}

template <typename T>
T Power(const T& arg, int power){
    using std::pow;

    return pow(arg, power);
}


template<typename T>
T prodpow(const T& x, std::size_t n){
    T result (1);
    for (std::size_t i = 0; i < n; i++){result *=x;}
    return result;
}


} // namespace Integrals
} // namespace Caravel

// External Instantiations
#include "Power.hpp"

#endif // _POWER_H_
