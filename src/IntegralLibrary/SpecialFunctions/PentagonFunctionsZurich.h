#pragma once

#include <vector>
#include <complex>
#include <string>
#include <iostream>
#include <stdexcept>

#include "Core/PhysRegSelector.h"

// Forward declare the functions we are using as they do not provide
// dedicated header files.
std::complex<double> evaluatelower(int whichint, double s12, double s23, double s34, double s45, double s15);
std::complex<double> integrate(int whichint, double s12, double s23, double s34, double s45, double s15, double err,
                               size_t iwsalloc);

namespace Caravel {
/// Contains everything related to the Masters.
namespace Integrals {

// Precision for the numerical integrations.
constexpr double _PRECISION = 1e-12;

/**
 * The pentagon function \f$ f_{1,1} \f$ as defined in equations (5.4) and
 * (5.5) of \cite Gehrmann:2018yef.
 * This is a logarithm.
 *
 * The generalized implementation will throw an exception because the
 * pentagon functions are only available for double input and complex
 * output for which dedicated specialisations are available.
 *
 * @param  v1 The first invariant.
 * @return    The pentagon function evaluated for the given input.
 */
template <typename Out, typename In>
Out pentagon_f_1_1(const In& v1);

// ----------------------------------------------------------------------------

/**
 * The pentagon function \f$ f_{2,1} \f$ as defined in equations (5.6) of
 * \cite Gehrmann:2018yef.
 *
 * The generalized implementation will throw an exception because the
 * pentagon functions are only available for double input and complex
 * output for which dedicated specialisations are available.
 *
 * @param  v1 The first invariant.
 * @param  v3 The third invariant.
 * @return    The pentagon function evaluated for the given input.
 */
template <typename Out, typename In>
Out pentagon_f_2_1(const In& v1, const In& v3);

// ----------------------------------------------------------------------------

/**
 * The pentagon function \f$ f_{3,1} \f$ as defined in equations (5.10) of
 * \cite Gehrmann:2018yef.
 *
 * The generalized implementation will throw an exception because the
 * pentagon functions are only available for double input and complex
 * output for which dedicated specialisations are available.
 *
 * @param  v1 The first invariant.
 * @param  v3 The third invariant.
 * @return    The pentagon function evaluated for the given input.
 */
template <typename Out, typename In>
Out pentagon_f_3_1(const In& v1, const In& v3);

// ----------------------------------------------------------------------------

/**
 * The pentagon function \f$ f_{3,2} \f$ as defined in equations (5.11) of
 * \cite Gehrmann:2018yef.
 *
 * The generalized implementation will throw an exception because the
 * pentagon functions are only available for double input and complex
 * output for which dedicated specialisations are available.
 *
 * @param  v1 The first invariant.
 * @param  v3 The third invariant.
 * @return    The pentagon function evaluated for the given input.
 */
template <typename Out, typename In>
Out pentagon_f_3_2(const In& v1, const In& v3);

// ----------------------------------------------------------------------------

/**
 * The pentagon function \f$ f_{3,3} \f$ as defined in equations (5.14) of
 * \cite Gehrmann:2018yef.
 *
 * The generalized implementation will throw an exception because the
 * pentagon functions are only available for double input and complex
 * output for which dedicated specialisations are available.
 *
 * @param  v1 The first invariant.
 * @param  v2 The second invariant.
 * @param  v4 The fourth invariant.
 * @return    The pentagon function evaluated for the given input.
 */
template <typename Out, typename In>
Out pentagon_f_3_3(const In& v1, const In& v2, const In& v4);

// ----------------------------------------------------------------------------

/**
 * The pentagon function \f$ f_{3,4} \f$ as defined in equations (5.16) of
 * \cite Gehrmann:2018yef.
 *
 * The generalized implementation will throw an exception because the
 * pentagon functions are only available for double input and complex
 * output for which dedicated specialisations are available.
 *
 * @param  v1   The first invariant.
 * @param  v2   The second invariant.
 * @param  v3   The third invariant.
 * @param  v4   The fourth invariant.
 * @param  v5   The fifth invariant.
 * @param  str5 The sign of \f$ tr_5 \f$
 * @return    The pentagon function evaluated for the given input.
 */
template <typename Out, typename In>
Out pentagon_f_3_4(const In& v1, const In& v2, const In& v3, const In& v4, const In& v5, const In& str5);

// ----------------------------------------------------------------------------

/**
 * The pentagon function \f$ f_{4,1} \f$ as defined in
 * \cite Gehrmann:2018yef.
 *
 * The generalized implementation will throw an exception because the
 * pentagon functions are only available for double input and complex
 * output for which dedicated specialisations are available.
 *
 * @param  v1 The first invariant.
 * @param  v3 The third invariant.
 * @param  string associated to v1.
 * @param  string associated to v3.
 * @return    The pentagon function evaluated for the given input.
 */
template <typename Out, typename In>
Out pentagon_f_4_1(const In& v1, const In& v3, const std::string& = "EUCLIDEAN", const std::string& = "EUCLIDEAN");

// ----------------------------------------------------------------------------

/**
 * The pentagon function \f$ f_{4,2} \f$ as defined in
 * \cite Gehrmann:2018yef.
 *
 * The generalized implementation will throw an exception because the
 * pentagon functions are only available for double input and complex
 * output for which dedicated specialisations are available.
 *
 * @param  v1 The first invariant.
 * @param  v3 The third invariant.
 * @param  string associated to v1.
 * @param  string associated to v3.
 * @return    The pentagon function evaluated for the given input.
 */
template <typename Out, typename In>
Out pentagon_f_4_2(const In& v1, const In& v3, const std::string& = "EUCLIDEAN", const std::string& = "EUCLIDEAN");

// ----------------------------------------------------------------------------

/**
 * The pentagon function \f$ f_{4,3} \f$ as defined in
 * \cite Gehrmann:2018yef.
 *
 * The generalized implementation will throw an exception because the
 * pentagon functions are only available for double input and complex
 * output for which dedicated specialisations are available.
 *
 * @param  v1 The first invariant.
 * @param  v3 The third invariant.
 * @param  string associated to v1.
 * @param  string associated to v3.
 * @return    The pentagon function evaluated for the given input.
 */
template <typename Out, typename In>
Out pentagon_f_4_3(const In& v1, const In& v3, const std::string& = "EUCLIDEAN", const std::string& = "EUCLIDEAN");

// ----------------------------------------------------------------------------

/**
 * The pentagon function \f$ f_{4,4} \f$ as defined in
 * \cite Gehrmann:2018yef.
 *
 * The generalized implementation will throw an exception because the
 * pentagon functions are only available for double input and complex
 * output for which dedicated specialisations are available.
 *
 * @param  v1 The first invariant.
 * @param  v2 The second invariant.
 * @param  v4 The fourth invariant.
 * @return    The pentagon function evaluated for the given input.
 */
template <typename Out, typename In>
Out pentagon_f_4_4(const In& v1, const In& v2, const In& v4);

// ----------------------------------------------------------------------------

/**
 * The pentagon function \f$ f_{4,5} \f$ as defined in
 * \cite Gehrmann:2018yef.
 *
 * The generalized implementation will throw an exception because the
 * pentagon functions are only available for double input and complex
 * output for which dedicated specialisations are available.
 *
 * @param  v1 The first invariant.
 * @param  v2 The second invariant.
 * @param  v4 The fourth invariant.
 * @return    The pentagon function evaluated for the given input.
 */
template <typename Out, typename In>
Out pentagon_f_4_5(const In& v1, const In& v2, const In& v4);

// ----------------------------------------------------------------------------

/**
 * The pentagon function \f$ f_{4,6} \f$ as defined in
 * \cite Gehrmann:2018yef.
 *
 * The generalized implementation will throw an exception because the
 * pentagon functions are only available for double input and complex
 * output for which dedicated specialisations are available.
 *
 * @param  v1 The first invariant.
 * @param  v3 The third invariant.
 * @param  v5 The fifth invariant.
 * @return    The pentagon function evaluated for the given input.
 */
template <typename Out, typename In>
Out pentagon_f_4_6(const In& v1, const In& v3, const In& v5);

// ----------------------------------------------------------------------------

/**
 * The pentagon function \f$ f_{4,7} \f$ as defined in
 * \cite Gehrmann:2018yef.
 *
 * The generalized implementation will throw an exception because the
 * pentagon functions are only available for double input and complex
 * output for which dedicated specialisations are available.
 *
 * @param  v1 The first invariant.
 * @param  v3 The third invariant.
 * @param  v4 The fourth invariant.
 * @return    The pentagon function evaluated for the given input.
 */
template <typename Out, typename In>
Out pentagon_f_4_7(const In& v1, const In& v3, const In& v4);

// ----------------------------------------------------------------------------

/**
 * The pentagon function \f$ f_{4,8} \f$ as defined in
 * \cite Gehrmann:2018yef.
 *
 * The generalized implementation will throw an exception because the
 * pentagon functions are only available for double input and complex
 * output for which dedicated specialisations are available.
 *
 * @param  v2 The second invariant.
 * @param  v4 The fourth invariant.
 * @param  v5 The fifth invariant.
 * @return    The pentagon function evaluated for the given input.
 */
template <typename Out, typename In>
Out pentagon_f_4_8(const In& v2, const In& v4, const In& v5);

// ----------------------------------------------------------------------------

/**
 * The pentagon function \f$ f_{4,9} \f$ as defined in
 * \cite Gehrmann:2018yef.
 *
 * The generalized implementation will throw an exception because the
 * pentagon functions are only available for double input and complex
 * output for which dedicated specialisations are available.
 *
 * @param  v1 The first invariant.
 * @param  v3 The third invariant.
 * @param  v4 The fourth invariant.
 * @return    The pentagon function evaluated for the given input.
 */
template <typename Out, typename In>
Out pentagon_f_4_9(const In& v1, const In& v3, const In& v4);

// ----------------------------------------------------------------------------

/**
 * The pentagon function \f$ f_{4,10} \f$ as defined in
 * \cite Gehrmann:2018yef.
 *
 * The generalized implementation will throw an exception because the
 * pentagon functions are only available for double input and complex
 * output for which dedicated specialisations are available.
 *
 * @param  v1   The first invariant.
 * @param  v2   The second invariant.
 * @param  v3   The third invariant.
 * @param  v4   The fourth invariant.
 * @param  v5   The fifth invariant.
 * @param  str5 The sign of \f$ tr_5 \f$
 * @return    The pentagon function evaluated for the given input.
 */
template <typename Out, typename In>
Out pentagon_f_4_10(const In& v1, const In& v2, const In& v3, const In& v4, const In& v5, const In& str5);

// ----------------------------------------------------------------------------

/**
 * The pentagon function \f$ f_{4,11} \f$ as defined in
 * \cite Gehrmann:2018yef.
 *
 * The generalized implementation will throw an exception because the
 * pentagon functions are only available for double input and complex
 * output for which dedicated specialisations are available.
 *
 * @param  v1 The first invariant.
 * @param  v2 The second invariant.
 * @param  v3 The third invariant.
 * @param  v4 The fourth invariant.
 * @param  v5 The fifth invariant.
 * @return    The pentagon function evaluated for the given input.
 */
template <typename Out, typename In>
Out pentagon_f_4_11(const In& v1, const In& v2, const In& v3, const In& v4, const In& v5);

// ----------------------------------------------------------------------------

/**
 * The pentagon function \f$ f_{4,12} \f$ as defined in
 * \cite Gehrmann:2018yef.
 *
 * The generalized implementation will throw an exception because the
 * pentagon functions are only available for double input and complex
 * output for which dedicated specialisations are available.
 *
 * @param  v1   The first invariant.
 * @param  v2   The second invariant.
 * @param  v3   The third invariant.
 * @param  v4   The fourth invariant.
 * @param  v5   The fifth invariant.
 * @param  str5 The sign of \f$ tr_5 \f$
 * @return    The pentagon function evaluated for the given input.
 */
template <typename Out, typename In>
Out pentagon_f_4_12(const In& v1, const In& v2, const In& v3, const In& v4, const In& v5, const In& str5);

} // namespace Integrals
} // namespace Caravel

// Implementations
//#include "PentagonFunctionsZurich.hpp"
