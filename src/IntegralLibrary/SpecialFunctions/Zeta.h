#ifndef _ZETA_H_
#define _ZETA_H_

#include "IntegralLibrary/CLN_QD_conversion.h"

#include "ginac/ginac.h"
#include <stdexcept>

namespace Caravel {
namespace Integrals {

// -------------------- Zeta_2 for generic types -----------------------------

/// \f$ \zeta_2 \f$ for generic types.
template <typename T>
const T _zeta2;



// -------------------- Zeta_3 for generic types -----------------------------

/// \f$ \zeta_3 \f$ for generic types.
template <typename T>
const T _zeta3;

/// \f$ \zeta_5 \f$ for generic types.
template <typename T>
const T _zeta5;



template <typename T>
T zeta(int arg) {
    if (arg == 2) {
        return _zeta2<T>;
    } else if (arg == 3) {
        return _zeta3<T>;
    } else if (arg == 5) {
        return _zeta5<T>;
    } else {
        throw std::runtime_error("Only Zeta2, Zeta3 and Zeta5 are implemented");
    }
}

} // namespace Integrals
} // namespace Caravel

// External Instantiations
#include "Zeta.hpp"

#endif // _ZETA_H_
