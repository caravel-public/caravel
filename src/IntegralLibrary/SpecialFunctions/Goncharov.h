#ifndef _GONCHAROV_H_
#define _GONCHAROV_H_

#include <iostream>

#include "IntegralLibrary/CLN_QD_conversion.h"

#include "ginac/ginac.h"

namespace Caravel {
namespace Integrals {

template <typename T>
T myabs(const T& val){
    return (val > T(0)) ? val : T(-1) * val;
}

/// Set the number of digits in GiNaC depending on the input type.
template <typename T>
void set_digits();


/// Check whether a number is considered to be zero.
template <typename T>
bool is_zero(const T& in);


/// Implementation of a MPL of weight 1.
template <typename Out, typename In>
Out G(const In& a1, const In& a2) {
    try {
        set_digits<In>();
        auto b1 = GiNaC::numeric(qd_to_cln(a1));
        auto b2 = GiNaC::numeric(qd_to_cln(a2));
        auto b1Db2 = GiNaC::numeric(qd_to_cln(a1) / qd_to_cln(a2));

        // Case of a simple logarithm
        if (b1 == GiNaC::numeric("0")) {
            return cln_to_qd<Out>(GiNaC::ex_to<GiNaC::numeric>(GiNaC::log(b2).evalf()).to_cl_N());
            // Rescale the arguments
        } else if (b2 != GiNaC::numeric("0")) {
            return cln_to_qd<Out>(GiNaC::ex_to<GiNaC::numeric>(GiNaC::G(b1Db2, GiNaC::numeric(1.)).evalf()).to_cl_N());
        } else {
            return cln_to_qd<Out>(GiNaC::ex_to<GiNaC::numeric>(GiNaC::G(b1, b2).evalf()).to_cl_N());
        }
    } catch (...) {
        std::cout << "a1 = " << a1 << "  a2 = " << a2 << std::endl;
        throw;
        // return Out(0);
    }
}

/// Implementation of a MPL of weight 2.
///
/// GiNaC assumes that there are no trailing zeros. If there are, the code
/// calls a linear combination of Goncharov Polylogarithms obtained through
/// shuffling relations that are hard-coded.
template <typename Out, typename In>
Out G(const In& a1, const In& a2, const In& a3) {

    try {
        set_digits<In>();

        // Perform the shuffling
        if (is_zero(a2) && !is_zero(a1) && !is_zero(a3)) {
            return G<Out>(In(0), a3) * G<Out>(a1, a3) - G<Out>(In(0), a1, a3);
        } else {
            auto b1 = GiNaC::numeric(qd_to_cln(a1));
            auto b2 = GiNaC::numeric(qd_to_cln(a2));
            auto b3 = GiNaC::numeric(qd_to_cln(a3));
            auto b1Db3 = GiNaC::numeric(qd_to_cln(a1) / qd_to_cln(a3));
            auto b2Db3 = GiNaC::numeric(qd_to_cln(a2) / qd_to_cln(a3));

            // Logarithm
            if (b1 == GiNaC::numeric("0") && b2 == GiNaC::numeric("0")) {
                return cln_to_qd<Out>(
                    GiNaC::ex_to<GiNaC::numeric>(
                        (GiNaC::numeric("1") / GiNaC::numeric("2") * GiNaC::log(b3) * GiNaC::log(b3)).evalf())
                        .to_cl_N());
                // Rescaling
            } else if (b2 != GiNaC::numeric("0") && b3 != GiNaC::numeric("0")) {
                return cln_to_qd<Out>(
                    GiNaC::ex_to<GiNaC::numeric>(GiNaC::G(GiNaC::lst({b1Db3, b2Db3}), GiNaC::numeric("1")).evalf())
                        .to_cl_N());
            } else {
                return cln_to_qd<Out>(
                    GiNaC::ex_to<GiNaC::numeric>(GiNaC::G(GiNaC::lst({b1, b2}), b3).evalf()).to_cl_N());
            }
        }
    } catch (...) {
        std::cout << "a1 = " << a1 << "  a2 = " << a2 << "  a3 = " << a3 << std::endl;
        throw;
        // return Out(0);
    }
}

/// Implementation of a MPL of weight 3.
///
/// GiNaC assumes that there are no trailing zeros. If there are, the code
/// calls a linear combination of Goncharov Polylogarithms obtained through
/// shuffling relations that are hard-coded.
template <typename Out, typename In>
Out G(const In& a1, const In& a2, const In& a3, const In& a4) {
    try {
        set_digits<In>();

        // Shuffling
        if (is_zero(a2) && is_zero(a3) && !is_zero(a1) && !is_zero(a4)) {
            return G<Out>(a1, a4) * G<Out>(In(0), In(0), a4) - G<Out>(In(0), a4) * G<Out>(In(0), a1, a4) +
                   G<Out>(In(0), In(0), a1, a4);
            // Shuffling
        } else if (is_zero(a3) && !is_zero(a1) && !is_zero(a2) && !is_zero(a4)) {
            return G<Out>(In(0), a4) * G<Out>(a1, a2, a4) - G<Out>(In(0), a1, a2, a4) - G<Out>(a1, In(0), a2, a4);
            // Shuffling
        } else if (is_zero(a1) && !is_zero(a2) && is_zero(a3) && !is_zero(a4)) {
            return G<Out>(In(0), a4) * G<Out>(In(0), a2, a4) - Out(2) * G<Out>(In(0), In(0), a2, a4);
        } else {
            auto b1 = GiNaC::numeric(qd_to_cln(a1));
            auto b2 = GiNaC::numeric(qd_to_cln(a2));
            auto b3 = GiNaC::numeric(qd_to_cln(a3));
            auto b4 = GiNaC::numeric(qd_to_cln(a4));
            auto zero = GiNaC::numeric("0");
            auto logterm = GiNaC::log(b4);
            auto b1Db4 = GiNaC::numeric(qd_to_cln(a1) / qd_to_cln(a4));
            auto b2Db4 = GiNaC::numeric(qd_to_cln(a2) / qd_to_cln(a4));
            auto b3Db4 = GiNaC::numeric(qd_to_cln(a3) / qd_to_cln(a4));

            // Logarithm
            if (b1 == zero && b2 == zero && b3 == zero) {
                return cln_to_qd<Out>(
                    GiNaC::ex_to<GiNaC::numeric>(
                        (GiNaC::numeric("1") / GiNaC::numeric("6") * logterm * logterm * logterm).evalf())
                        .to_cl_N());
                // Rescaling
            } else if (b3 != zero && b4 != zero) {
                return cln_to_qd<Out>(GiNaC::ex_to<GiNaC::numeric>(
                                          GiNaC::G(GiNaC::lst({b1Db4, b2Db4, b3Db4}), GiNaC::numeric("1")).evalf())
                                          .to_cl_N());
            } else {
                return cln_to_qd<Out>(
                    GiNaC::ex_to<GiNaC::numeric>(GiNaC::G(GiNaC::lst({b1, b2, b3}), b4).evalf()).to_cl_N());
            }
        }

    } catch (...) {
        std::cout << "a1 = " << a1 << "  a2 = " << a2 << "  a3 = " << a3 << "  a4 = " << a4 << std::endl;
        throw;
        // return Out(0);
    }
}

/// Implementation of a MPL of weight 4.
///
/// GiNaC assumes that there are no trailing zeros. If there are, the code
/// calls a linear combination of Goncharov Polylogarithms obtained through
/// shuffling relations that are hard-coded.
template <typename Out, typename In>
Out G(const In& a1, const In& a2, const In& a3, const In& a4, const In& a5) {
    try {
        set_digits<In>();

        // Shuffling
        if (!is_zero(a1) && !is_zero(a2) && !is_zero(a3) && is_zero(a4) && !is_zero(a5)) {
            return G<Out>(In(0), a5) * G<Out>(a1, a2, a3, a5) - G<Out>(In(0), a1, a2, a3, a5) -
                   G<Out>(a1, In(0), a2, a3, a5) - G<Out>(a1, a2, In(0), a3, a5);
            // Shuffling
        } else if (!is_zero(a1) && !is_zero(a2) && is_zero(a3) && is_zero(a4) && !is_zero(a5)) {
            return G<Out>(In(0), In(0), a5) * G<Out>(a1, a2, a5) -
                   G<Out>(In(0), a5) * (G<Out>(In(0), a1, a2, a5) + G<Out>(a1, In(0), a2, a5)) +
                   G<Out>(In(0), In(0), a1, a2, a5) + G<Out>(In(0), a1, In(0), a2, a5) +
                   G<Out>(a1, In(0), In(0), a2, a5);
        } else if (!is_zero(a1) && is_zero(a2) && is_zero(a3) && is_zero(a4) && !is_zero(a5)) {
            return -G<Out>(In(0), In(0), a5) * G<Out>(In(0), a1, a5) +
                   G<Out>(a1, a5) * G<Out>(In(0), In(0), In(0), a5) + G<Out>(In(0), a5) * G<Out>(In(0), In(0), a1, a5) -
                   G<Out>(In(0), In(0), In(0), a1, a5);
        } else if (!is_zero(a1) && is_zero(a2) && !is_zero(a3) && is_zero(a4) && !is_zero(a5)) {
            return G<Out>(In(0), a5) * G<Out>(a1, In(0), a3, a5) - G<Out>(In(0), a1, In(0), a3, a5) -
                   Out(2) * G<Out>(a1, In(0), In(0), a3, a5);
        } else if (is_zero(a1) && !is_zero(a2) && is_zero(a3) && is_zero(a4) && !is_zero(a5)) {
            return G<Out>(In(0), In(0), a5) * G<Out>(In(0), a2, a5) -
                   Out(2) * G<Out>(In(0), a5) * G<Out>(In(0), In(0), a2, a5) +
                   Out(3) * G<Out>(In(0), In(0), In(0), a2, a5);
        } else if (is_zero(a1) && !is_zero(a2) && !is_zero(a3) && is_zero(a4) && !is_zero(a5)) {
	    return G<Out>(In(0), a5) * G<Out>(In(0), a2, a3, a5) - G<Out>(In(0), a2, In(0), a3, a5) -
	           Out(2) * G<Out>(In(0), In(0), a2, a3, a5);
        } else if (is_zero(a1) && is_zero(a2) && !is_zero(a3) && is_zero(a4) && !is_zero(a5)) {
	    return G<Out>(In(0), a5) * G<Out>(In(0), In(0), a3, a5) -
	           Out(3) * G<Out>(In(0), In(0), In(0), a3, a5);
        } else {
            auto b1 = GiNaC::numeric(qd_to_cln(a1));
            auto b2 = GiNaC::numeric(qd_to_cln(a2));
            auto b3 = GiNaC::numeric(qd_to_cln(a3));
            auto b4 = GiNaC::numeric(qd_to_cln(a4));
            auto b5 = GiNaC::numeric(qd_to_cln(a5));
            auto b1Db5 = GiNaC::numeric(qd_to_cln(a1) / qd_to_cln(a5));
            auto b2Db5 = GiNaC::numeric(qd_to_cln(a2) / qd_to_cln(a5));
            auto b3Db5 = GiNaC::numeric(qd_to_cln(a3) / qd_to_cln(a5));
            auto b4Db5 = GiNaC::numeric(qd_to_cln(a4) / qd_to_cln(a5));
            auto zero = GiNaC::numeric("0");
            auto logterm = GiNaC::log(b5);

            // Logarithm
            if (b1 == zero && b2 == zero && b3 == zero && b4 == zero) {
                return cln_to_qd<Out>(
                    GiNaC::ex_to<GiNaC::numeric>(
                        (GiNaC::numeric("1") / GiNaC::numeric("24") * logterm * logterm * logterm * logterm).evalf())
                        .to_cl_N());
                // Rescaling
            } else if (b4 != zero && b5 != zero) {
                return cln_to_qd<Out>(
                    GiNaC::ex_to<GiNaC::numeric>(
                        GiNaC::G(GiNaC::lst({b1Db5, b2Db5, b3Db5, b4Db5}), GiNaC::numeric("1")).evalf())
                        .to_cl_N());
            } else {
                return cln_to_qd<Out>(
                    GiNaC::ex_to<GiNaC::numeric>(
                        GiNaC::G(GiNaC::lst({GiNaC::numeric(qd_to_cln(a1)), GiNaC::numeric(qd_to_cln(a2)),
                                             GiNaC::numeric(qd_to_cln(a3)), GiNaC::numeric(qd_to_cln(a4))}),
                                 GiNaC::numeric(qd_to_cln(a5)))
                            .evalf())
                        .to_cl_N());
            }
        }

    } catch (...) {
        std::cout << "a1 = " << a1 << "  a2 = " << a2 << "  a3 = " << a3 << "  a4 = " << a4 << "  a5 = " << a5
                  << std::endl;
        throw;
        // return Out(0);
    }
}

template <typename Out, typename In>
Out G(const In& a1, const In& a2, const In& a3, const In& a4, const In& a5, const In& a6) {
    try {
        set_digits<In>();

        auto zero = GiNaC::numeric("0");
        auto b1 = GiNaC::numeric(qd_to_cln(a1));
        auto b2 = GiNaC::numeric(qd_to_cln(a2));
        auto b3 = GiNaC::numeric(qd_to_cln(a3));
        auto b4 = GiNaC::numeric(qd_to_cln(a4));
        auto b5 = GiNaC::numeric(qd_to_cln(a5));
        auto b6 = GiNaC::numeric(qd_to_cln(a6));
        auto logterm = GiNaC::log(b6);

        if(b1 == zero && b2 == zero && b3 == zero && b4 == zero && b5 == zero){
            return cln_to_qd<Out>(
                GiNaC::ex_to<GiNaC::numeric>(
                    (GiNaC::numeric("1") / GiNaC::numeric("120") * logterm * logterm * logterm * logterm * logterm).evalf())
                    .to_cl_N());
        }
        return cln_to_qd<Out>(GiNaC::ex_to<GiNaC::numeric>(
                                  GiNaC::G(GiNaC::lst({GiNaC::numeric(qd_to_cln(a1)), GiNaC::numeric(qd_to_cln(a2)),
                                                       GiNaC::numeric(qd_to_cln(a3)), GiNaC::numeric(qd_to_cln(a4)),
                                                       GiNaC::numeric(qd_to_cln(a5))}),
                                           GiNaC::numeric(qd_to_cln(a6)))
                                      .evalf())
                                  .to_cl_N());
    } catch (...) {
        std::cout << "a1 = " << a1 << "  a2 = " << a2 << "  a3 = " << a3 << "  a4 = " << a4 << "  a5 = " << a5
                  << "  a6 = " << a6 << std::endl;
        throw;
        // return Out(0);
    }
}

template <typename Out, typename In>
Out G(const In& a1, const In& a2, const In& a3, const In& a4, const In& a5, const In& a6, const In& a7) {
    try {
        set_digits<In>();

        auto zero = GiNaC::numeric("0");
        auto b1 = GiNaC::numeric(qd_to_cln(a1));
        auto b2 = GiNaC::numeric(qd_to_cln(a2));
        auto b3 = GiNaC::numeric(qd_to_cln(a3));
        auto b4 = GiNaC::numeric(qd_to_cln(a4));
        auto b5 = GiNaC::numeric(qd_to_cln(a5));
        auto b6 = GiNaC::numeric(qd_to_cln(a6));
        auto b7 = GiNaC::numeric(qd_to_cln(a7));
        auto logterm = GiNaC::log(b7);

        if(b1 == zero && b2 == zero && b3 == zero && b4 == zero && b5 == zero && b6 == zero){
            return cln_to_qd<Out>(
                GiNaC::ex_to<GiNaC::numeric>(
                    (GiNaC::numeric("1") / GiNaC::numeric("720") * logterm * logterm * logterm * logterm * logterm * logterm).evalf())
                    .to_cl_N());
        }

        return cln_to_qd<Out>(GiNaC::ex_to<GiNaC::numeric>(
                                  GiNaC::G(GiNaC::lst({GiNaC::numeric(qd_to_cln(a1)), GiNaC::numeric(qd_to_cln(a2)),
                                                       GiNaC::numeric(qd_to_cln(a3)), GiNaC::numeric(qd_to_cln(a4)),
                                                       GiNaC::numeric(qd_to_cln(a5)), GiNaC::numeric(qd_to_cln(a6))}),
                                           GiNaC::numeric(qd_to_cln(a7)))
                                      .evalf())
                                  .to_cl_N());
    }
    catch(...){
        std::cout << "a1 = " << a1 << "  a2 = " << a2 << "  a3 = " << a3 << "  a4 = " << a4 << "  a5 = " << a5
                  << "  a6 = " << a6 << "   a7 = " << a7 << std::endl;
        throw;
        // return Out(0);
    }
}

} // namespace Integrals
} // namespace Caravel

// External instantiations
#include "Goncharov.hpp"

#endif
