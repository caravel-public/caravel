#include "InitialConditions.h"

namespace Caravel {
namespace Integrals {

template <> R finBoxTriInit<R>(const R& str5) { return str5 * 105.88442637717993; }

template <> C finBoxTriInit<C>(const C& str5) { return C(finBoxTriInit<R>(str5.real()), R(0)); }

template <> R finPentaBubInit<R>(const R& str5) { return str5 * 42.86978685275925; }

template <> C finPentaBubInit<C>(const C& str5) { return C(finPentaBubInit<R>(str5.real()), R(0)); }

template <> R pentw4Init<R>(const R& str5) { return str5 * 21.864907011285677; }

template <> C pentw4Init<C>(const C& str5) { return C(pentw4Init<R>(str5.real()), R(0)); }

} // namespace Integrals
} // namespace Caravel