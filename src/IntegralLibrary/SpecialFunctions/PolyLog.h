#ifndef _POLYLOG_H_
#define _POLYLOG_H_

#include <complex>
#include <iostream>
using std::cout;
using std::endl;

#include "IntegralLibrary/CLN_QD_conversion.h"

#include "ginac/ginac.h"

namespace Caravel {
namespace Integrals {

template <typename Out, typename In>
Out PolyLog(int n, const In& z){
    return cln_to_qd<Out>(
        GiNaC::ex_to<GiNaC::numeric>(
            GiNaC::Li(GiNaC::numeric(n),
                      GiNaC::numeric(qd_to_cln(z))).evalf()
        ).to_cl_N());
}

/// Li22 interface
template <typename T>
T L22(const T& a, const T& b){
    return cln_to_qd<T>(
        GiNaC::ex_to<GiNaC::numeric>(
            GiNaC::Li(GiNaC::lst({2, 2}),
                      GiNaC::lst({GiNaC::numeric(qd_to_cln(a)),
                       GiNaC::numeric(qd_to_cln(b))})).evalf()
        ).to_cl_N());
}

} // namespace Integrals
} // namespace Caravel

// External Instantiations
#include "PolyLog.hpp"

#endif // _POLYLOG_H_
