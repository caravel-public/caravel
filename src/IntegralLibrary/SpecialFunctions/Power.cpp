#include "Power.h"


namespace Caravel {
namespace Integrals {

template <>
double Power(const double& arg, int power){
    return std::pow(arg, power);
}

// ----------------------------------------------------------------------------
//                              Instantiations
// ----------------------------------------------------------------------------
#define INSTANTIATE_POWER(T) \
    template T prodpow(const T&, size_t);

INSTANTIATE_POWER(R)
#ifdef HIGH_PRECISION
INSTANTIATE_POWER(RHP)
#endif
#ifdef VERY_HIGH_PRECISION
INSTANTIATE_POWER(RVHP)
#endif

INSTANTIATE_POWER(C)
#ifdef HIGH_PRECISION
INSTANTIATE_POWER(CHP)
#endif
#ifdef VERY_HIGH_PRECISION
INSTANTIATE_POWER(CVHP)
#endif

} // namespace Integrals
} // namespace Caravel
