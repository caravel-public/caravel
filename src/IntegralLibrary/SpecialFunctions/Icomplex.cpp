#include "Icomplex.h"
#if defined(HIGH_PRECISION) || defined(VERY_HIGH_PRECISION)
#include "qd/qd_real.h"
#endif
#include <complex>

namespace Caravel {
namespace Integrals {

template <>
const C icomplex<C> = C(R(0.0), R(1.0));

#ifdef HIGH_PRECISION
template <>
const CHP icomplex<CHP> = CHP(RHP("0.0"), RHP("1.0"));
#endif

#ifdef VERY_HIGH_PRECISION
template <>
const CVHP icomplex<CVHP> = CVHP(RVHP("0.0"), RVHP("1.0"));
#endif

} // namespace Integrals
} // namespace Caravel
