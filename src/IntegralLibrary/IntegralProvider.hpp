
namespace Caravel{
namespace Integrals {

using ::Caravel::operator<<;

template <typename CType, typename FType>
IntegralProvider<CType, FType>::IntegralProvider(const std::vector<lGraph::IntegralGraph>& intGr) : mu_square(typename FType::input_type(1)), integral_graphs(intGr) {
    if(!loader){
#if defined(WITH_PENTAGONS)
        if (settings::integrals::integral_family_1L.value == settings::integrals::family_types_1l::pentagon_functions)
            Integrals::loop_1::PentagonFunctions::load();
        if (settings::integrals::integral_family_2L.value == settings::integrals::family_types_2l::pentagon_functions)
            Integrals::loop_2::MasslessPentagon::load();
#endif
#if defined(WITH_PENTAGONS_NEW)
	if (settings::integrals::integral_family_2L.value == settings::integrals::family_types_2l::pentagon_functions_new)
            Integrals::loop_2::PentagonFunctionsNew::load();
	if (settings::integrals::integral_family_1L.value == settings::integrals::family_types_1l::pentagon_functions_new)
            Integrals::loop_1::OneLoopPentagonFunctions::load();
#endif
#if defined(WITH_GONCHAROVS)
        if (settings::integrals::integral_family_1L.value == settings::integrals::family_types_1l::goncharovs)
            Integrals::loop_1::Goncharovs::load();
        if (settings::integrals::integral_family_2L.value == settings::integrals::family_types_2l::goncharovs)
            Integrals::loop_2::Goncharovs::load();
#endif
        loader=true;
    }
    construct_integrals();
}

template <typename CType, typename FType>
bool IntegralProvider<CType, FType>::loader = false;

template <typename CType, typename FType>
std::enable_if_t<!is_floating_point<typename CType::input_type>::value> 
    call_fill_integral_series(std::vector<std::function<Series<typename CType::output_type>(const momD_conf<typename CType::input_type, 4>&)>>& integral_series,
                                    const std::function<std::vector<momD<typename CType::input_type, 4>>(const momD_conf<typename CType::input_type, 4>&)>& conventions_map_ext_surface,
                                    const MasterIntegralInputGenerator<typename CType::input_type>& evaluator,
                                    const FunctionBasisExpansion<FBCoefficient<Series<typename CType::output_type>, typename CType::input_type>, FType>& loaded_integral) {
    using TC = typename CType::input_type;
    integral_series.push_back([](const momD_conf<TC, 4>& in) mutable {
        std::cout << "Dummy integral evaluation as it can not be evaluate in exact number fields, returns 0!" << std::endl;
        return Series<TC>(0, 0);
    });
}

template <typename CType, typename FType>
std::enable_if_t<is_floating_point<typename CType::input_type>::value> 
    call_fill_integral_series(std::vector<std::function<Series<typename CType::output_type>(const momD_conf<typename CType::input_type, 4>&)>>& integral_series,
                                    const std::function<std::vector<momD<typename CType::input_type, 4>>(const momD_conf<typename CType::input_type, 4>&)>& conventions_map_ext_surface,
                                    const MasterIntegralInputGenerator<typename CType::input_type>& evaluator,
                                    const FunctionBasisExpansion<FBCoefficient<Series<typename CType::output_type>, typename CType::input_type>, FType>& loaded_integral) {
    using TC = typename CType::input_type;
    using OutC = typename CType::output_type;
    std::function<Series<OutC>(const momD_conf<TC, 4>&)> ls = 
        [conventions_map_ext_surface, evaluator, loaded_integral](const momD_conf<TC, 4>& mc) mutable {
            auto input = evaluator(conventions_map_ext_surface(mc));
            return loaded_integral.eval_complete(input);
        };
    integral_series.push_back(ls);
}

void read_permutation_of_custom_basis_from_stream(std::ifstream&, std::unordered_map<lGraph::Graph<lGraph::Node,lGraph::Connection<lGraph::Strand>>, std::vector<size_t>>&);

template <typename CType, typename FType>
void fill_integral_containers(
    const lGraph::IntegralGraph& iG, std::vector<std::function<Series<typename CType::output_type>(const momD_conf<typename CType::input_type, 4>&)>>& integral_series,
    std::vector<std::function<NumericFunctionBasisExpansion<
        Series<typename CType::output_type>, FType>(const momD_conf<typename CType::input_type, 4>&)>>& integral_nfbe,
    const std::vector<typename FType::input_type>& masses, const typename FType::input_type& mu_square) {
    // Check we are not calling 'integrals_BH'
    if (settings::integrals::integral_family_1L.value == settings::integrals::family_types_1l::integrals_BH) {
        std::cerr<<"ERROR: for integrals::family_types_1l::integrals_BH use the ProviderBH!"<<std::endl;
        std::exit(1);
    }

    // The following block allows customization of integrand basis:
    //		Meant for application of symmetries to given diagrams.
    //		User needs to understand used basis and provided candidates
    static thread_local bool read_custom(true);
    // Map between:
    //	1: A base_graph (in an xGraph)
    //	2: The corresponding permutation of external momenta
    using TopoIDType = lGraph::Graph<lGraph::Node,lGraph::Connection<lGraph::Strand>>;
    static thread_local std::unordered_map<TopoIDType, std::vector<size_t>> custom_integrand_basis;
    // TODO: make more flexible later on (use settings, etc)
    std::string filename_custom = "integrand_basis_customize.dat";
    if(read_custom){
        read_custom=false;
        std::ifstream custom(filename_custom);
        if(custom.is_open()){
            // load information in map
            read_permutation_of_custom_basis_from_stream(custom, custom_integrand_basis);
        }
    }

    // std::cout<<"master_integral_data_holder size="<<master_integral_data_holder.size()<<std::endl;
    // std::cout<<"contents of master_integral_data_holder"<<std::endl;
    // for(auto&mi:master_integral_data_holder)
    //   std::cout<<mi.first<<std::endl;

    auto intdata = master_integral_data_holder.find(iG);

    // search for master-integral in corresponding map
    if (intdata==master_integral_data_holder.end()) {
        std::cerr << "ERROR: the IntegralProvider could not find in master_integral_data_holder the following integral: "<< std::endl;
        std::cerr << "Insertion: "<<iG.get_insertion_string()<<std::endl;
        iG.get_graph().show(std::cerr);
        std::cerr<< "These are the set integral types:\n	1 loop: "<<settings::integrals::integral_family_1L.value<<std::endl;
        std::cerr<< "	2 loops: "<<settings::integrals::integral_family_2L.value<<std::endl;
        std::exit(1);
    }

    using lGraph::xGraph;
    using TC = typename CType::input_type;
    using OutC = typename CType::output_type;
    using T = typename FType::input_type;
    using Out = typename FType::output_type;

    // extract information from integral map
    const auto& integral_cadidate_xg = std::get<xGraph>(intdata->second);
    const auto& creator = std::get<MasterIntegralPointer<OutC, TC, Out, T>>(intdata->second);
    const auto& evaluator = std::get<MasterIntegralInputGenerator<TC>>(intdata->second);

    auto conventions_map_ext_surface = lGraph::lGraphKin::external_momentum_mapper<TC, 4>(iG.get_graph(),integral_cadidate_xg);

    auto info_variables = creator.get_variables();

    // in case of customized basis
    std::vector<size_t> ext_permutation;
    for(size_t ii=0; ii<integral_cadidate_xg.get_n_legs();ii++)    ext_permutation.push_back(ii);
    auto replacement = custom_integrand_basis.find(iG.get_graph().get_base_graph());
    if(replacement != custom_integrand_basis.end())
        ext_permutation = replacement->second;

    auto tag = IntegralTagger(info_variables, ext_permutation, integral_cadidate_xg, iG.get_graph());
    DEBUG(
        std::cout<<"Will add the following integral:\n	insertion: "<<iG.get_insertion_string()<<", propagator structure:\n"<<iG.get_graph()<<std::endl;
        std::cout<<"Stored candidate: "<<std::endl;
        integral_cadidate_xg.show();
        std::cout<<"	info_variables: "<<info_variables<<std::endl;
        std::cout<<"	tag: "<<tag<<std::endl;
    );

    FunctionBasisExpansion<FBCoefficient<Series<OutC>, TC>, FType> loaded_integral = creator(tag.first, tag.second);

    call_fill_integral_series<CType, FType>(integral_series, conventions_map_ext_surface, evaluator, loaded_integral);

    using NFBExpansion = NumericFunctionBasisExpansion<Series<OutC>, FType>;
    std::function<NFBExpansion(const momD_conf<TC, 4>&)> ln = 
        [map = std::move(conventions_map_ext_surface), evaluate = std::move(evaluator), integral = std::move(loaded_integral)](const momD_conf<TC, 4>& mc) mutable {
            auto input = evaluate(map(mc));
            return integral.eval_coeffs(input);
        };
    integral_nfbe.push_back(ln);
}

template <typename CType, typename FType>
void IntegralProvider<CType, FType>::construct_integrals() {
    using T = typename FType::input_type;

    // allocate space for masses
    masses = std::vector<T>(ParticleMass::mass_index_global, T(0));
    // no storage for massless particles (as they are set to zero always)
    for (size_t ii = 0; ii < masses.size(); ii++) { masses[ii] = ParticleMass::particle_mass_container[ii].get_mass(T(1)); }

    // fill the lambdas
    for (size_t ii = 0; ii < integral_graphs.size(); ii++) fill_integral_containers<CType, FType>(integral_graphs[ii], integral_series, integral_nfbe, masses, mu_square);
}

template <typename CType, typename FType>
lGraph::IntegralContainer IntegralProvider<CType, FType>::get_integrals() const { return integrals; }

template <typename CType, typename FType>
std::vector<Series<typename CType::output_type>> IntegralProvider<CType, FType>::compute(const momD_conf<typename CType::input_type, 4>& in) const {
    using OutC = typename CType::output_type;

    std::vector<Series<OutC>> integral_values(integral_graphs.size());
    for (size_t ii = 0; ii < integral_series.size(); ii++) {
        integral_values[ii] = integral_series[ii](in);
        // std::cout<<"The integral_"<<ii<<": "<<integral_graphs[ii]<<" evaluates to: "<<integral_values[ii]<<std::endl;
    }
    return integral_values;
}

template <typename Cin, typename Fin>
std::enable_if_t<std::is_same<Cin, Fin>::value> SET_MU_HELPER(const Cin& m, Fin& mu_square){
    // TODO:
    if(m!=Fin(1))
        std::cout<<"WARNING: IntegralProvider<CType, FType>::set_mu_squared(.) still not setting renormalization scale!"<<std::endl;
    mu_square = m;
}

template <typename Cin, typename Fin>
std::enable_if_t<!std::is_same<Cin, Fin>::value> SET_MU_HELPER(const Cin& m, Fin& mu_square){
    // print warning if different to default
    if(m != Cin(1))
        std::cout<<"WARNING: IntegralProvider<CType, FType>::set_mu_squared(.) can not set mu when coeff and function types are different! mu received: "<<m<<std::endl;
}

template <typename CType, typename FType>
void IntegralProvider<CType, FType>::set_mu_squared(const typename CType::input_type& m) {
    using TC = typename CType::input_type;
    using T = typename FType::input_type;

    SET_MU_HELPER<TC, T>(m,mu_square);
}

template <typename CType, typename FType>
std::vector<
    NumericFunctionBasisExpansion<Series<typename CType::output_type>, FType>>
IntegralProvider<CType, FType>::get_abstract_integral(const momD_conf<typename CType::input_type, 4>& in) const {
    using OutC = typename CType::output_type;

    std::vector<NumericFunctionBasisExpansion<Series<OutC>, FType>> toret(integral_nfbe.size());

    for (size_t ii = 0; ii < integral_nfbe.size(); ii++) { toret[ii]=integral_nfbe[ii](in); }
    return toret;
}

template <typename CType, typename FType>
std::function<
  std::vector<
    NumericFunctionBasisExpansion<Series<typename CType::output_type>, FType>
  >(const momD_conf<typename CType::input_type, 4>& in)> IntegralProvider<CType, FType>::get_int_evaluator() const {
    using TC = typename CType::input_type;
    return [lintegral_nfbe=integral_nfbe](const momD_conf<TC, 4>& in){
            using OutC = typename CType::output_type;
            std::vector<NumericFunctionBasisExpansion<Series<OutC>, FType>> toret(lintegral_nfbe.size());
            for (size_t ii = 0; ii < lintegral_nfbe.size(); ii++) { toret[ii]=lintegral_nfbe[ii](in); }
            return toret;    
        };
}

// Declare instantiations in accordance with instantiation definitions in ProviderBH.cpp
#define _INST_INTEGRALPROVIDER(KEY, OutC, TC, Out, T) \
    KEY template class IntegralProvider<FBCoefficient<OutC, TC>, StdBasisFunction<Out, T>>;

_INST_INTEGRALPROVIDER(extern, C, R, C, R)
_INST_INTEGRALPROVIDER(extern, C, C, C, C)

#ifdef USE_FINITE_FIELDS
_INST_INTEGRALPROVIDER(extern, F32, F32, C, R)
_INST_INTEGRALPROVIDER(extern, F32, F32, C, C)
#endif

#ifdef HIGH_PRECISION
_INST_INTEGRALPROVIDER(extern, CHP, RHP, CHP, RHP)
_INST_INTEGRALPROVIDER(extern, CHP, CHP, CHP, CHP)

#ifdef USE_FINITE_FIELDS
_INST_INTEGRALPROVIDER(extern, F32, F32, CHP, RHP)
_INST_INTEGRALPROVIDER(extern, F32, F32, CHP, CHP)
#endif
#endif

#ifdef VERY_HIGH_PRECISION
_INST_INTEGRALPROVIDER(extern, CVHP, RVHP, CVHP, RVHP)
_INST_INTEGRALPROVIDER(extern, CVHP, CVHP, CVHP, CVHP)

#ifdef USE_FINITE_FIELDS
_INST_INTEGRALPROVIDER(extern, F32, F32, CVHP, RVHP)
_INST_INTEGRALPROVIDER(extern, F32, F32, CVHP, CVHP)
#endif
#endif


}
}
