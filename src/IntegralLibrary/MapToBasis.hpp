#include <fstream>
#include <stdexcept>
#include <sstream>

#include "Core/settings.h"
#include "Core/type_traits_extra.h"
#include "IntegralLibrary/CanonicalOrderingBasisFunctions.h"

namespace Caravel {
namespace Integrals {

// Construct with an unordered_map
template <typename T, typename FType>
MapToBasis<T, FType>::MapToBasis(
    const std::unordered_map<std::string, std::vector<std::pair<std::string, BigRat>>>& reps)
{
    for(auto&rep:reps) {
	std::vector<std::pair<std::string,std::function<T(const momentumD_configuration<T, 4>&,const bool)>>>lc;
	for(auto&term:rep.second) {
	    lc.push_back({term.first,[term](const momentumD_configuration<T, 4>&mcf,const bool negative_tr5) { return static_cast<T>(term.second);}});
	}
	replacements.insert({rep.first,lc});
    }
}

// Construct with an unordered_map
template <typename T, typename FType>
MapToBasis<T, FType>::MapToBasis(
    const std::unordered_map<std::string, std::vector<std::pair<std::string, std::function<T(const momentumD_configuration<T, 4>&,const bool)>>>>& reps)
    : replacements{reps} {}

template <typename T, typename FType>
MapToBasis<T, FType>::MapToBasis(const std::string& filename) {

    // Open the file.
    std::ifstream input(filename);

    // Check if it is openend correctly..
    if (input.fail()) {
        std::ostringstream oss;
        oss << "Failed to open file '" << filename << "'!\n"
            << "If you tried providing a process there is maybe no default file for your process "
            << "and you should try to provide a path to a custom file to the constructor.";
        throw std::runtime_error(oss.str());
    }

    // Extract the content of the file containing the replacement rules
    std::string content;
    std::getline(input, content);

    // Create a MathList from the content and loop over all the rules that it contains.
    MathList mlist(content, "List");
    std::vector<std::string> rules;
    for (const auto& rule : MathList(content, "List")) { replacements.insert(extract_replacement_rule(rule)); }

    // Close the file.
    input.close();
}

template <typename T, typename FType>
MapToBasis<T, FType>::MapToBasis(const PartialAmplitudeInput& process)
    : MapToBasis<T, FType>(MapToBasis<T, FType>::get_filename(process)) {}

template <typename T, typename FType>
NumericFunctionBasisExpansion<Series<T>, std::string> MapToBasis<T, FType>::
operator()(const NumericFunctionBasisExpansion<Series<T>, FType>& amplitude,momentumD_configuration<T,4> mcf,bool negative_tr5) const {

    // Create a clean basis
    std::unordered_map<std::string, Series<T>> clean_basis;

    // First add the functions that are in the basis.
    // These are the functions for which there is no replacement rule.
    for (const auto& entry : amplitude) {
        auto search = replacements.find(entry.first.as_string());
        if (search == replacements.end()) { clean_basis.insert({entry.first.as_string(), entry.second}); }
    }

    // Now add the functions that need replacements.
    for (const auto& entry : amplitude) {
        auto search = replacements.find(entry.first.as_string());

        // If there is a replacement rule
        if (search != replacements.end()) {
            // Store the coefficient of the function
            auto coeff = entry.second;

            // Loop over all the replacement terms
            for (const auto& rule : search->second) {
                auto lookfor = clean_basis.find(rule.first);
                // If it is in the basis, update the coefficient
                if (lookfor != clean_basis.end()) {
                    // The rational numbers we read from file cannot directly be converted to complex
                    // so we need to convert them first to the non-complex case. This is fine since 
                    // no complex numbers are read from the replacement rules.
                    lookfor->second += rule.second(mcf,negative_tr5) * coeff;
                }
                // if it is not yet in the basis, add it
                else {
                    clean_basis.insert({rule.first, rule.second(mcf,negative_tr5) * coeff});
                }
            }
        }
    }

    return NumericFunctionBasisExpansion<Series<T>, std::string>(clean_basis);
}

template <typename T, typename FType>
std::vector<std::pair<std::string, std::function<T(const momentumD_configuration<T, 4>&,const bool)>>>
MapToBasis<T, FType>::get_replacement_rule_for_function(const std::string& func) const {
    return replacements.at(func);
}

template <typename T, typename FType>
size_t MapToBasis<T, FType>::n_replacements() const {
    return replacements.size();
}

// Private methods.

template <typename T, typename FType>
std::string MapToBasis<T, FType>::extract_String_content(const std::string& s) {
    std::string tmp(std::string(s.begin() + 8, s.end() - 2));
    tmp = canonical_ordering_hash_string(tmp);
    return tmp;
}

template <typename T, typename FType>
std::vector<std::pair<std::string, std::function<T(const momentumD_configuration<T, 4>&, const bool)>>> MapToBasis<T, FType>::extract_replacing_lc(MathList&& reps) {

    std::vector<std::pair<std::string, std::function<T(const momentumD_configuration<T, 4>&, const bool)>>> toreturn;

    for (const auto& elem : reps) {
        MathList tmp(elem, "List");
        MathList rat_list(tmp.tail[1], "Rat");
        BigRat rat = BigRat(rat_list.tail[0].c_str()) / BigRat(rat_list.tail[1].c_str());
	std::function<T(const momentumD_configuration<T, 4>&,const bool)> fRat;
	if (tmp.size() == 2) {
	    fRat = [rat](const momentumD_configuration<T, 4>&mcf, const bool negative_tr5){
		       return static_cast<T>(static_cast<remove_complex_t<T>>(rat));
		   };
	} else if (tmp.size() == 3) {
	    MathList inv_list(tmp.tail[2], "List");
	    std::vector<std::vector<size_t>> ind;
	    for (const auto& inv : inv_list) {
	    	ind.emplace_back();
	    	MathList inv_terms(inv, "Inv");
	    	for (const auto& strInd : inv_terms.tail) {
	    	    ind.back().push_back(atoi(strInd.c_str()));
	    	}
	    }

	    fRat = [rat,ind](const momentumD_configuration<T, 4>&mcf, const bool negative_tr5){
		       std::vector<std::pair<size_t,size_t>> s{{1, 2}, {2, 3}, {3, 4}, {4, 5}, {1, 5}, {1, 3}, {2, 4}, {3, 5}, {1, 4}, {2, 5}};
		       T res = static_cast<T>(static_cast<remove_complex_t<T>>(rat));
		       for (auto& i : ind)
			   switch (i[0]){
			   case 1:
			       if (i[1] < 11)
				   res *= mcf.s(s[i[1]-1].first, s[i[1]-1].second);
			       else if ( i[1] == 11 ) {
				   if (negative_tr5)
				       res *= T(-1);
			       }
			       else
				   std::cout<<"WARNING: asked for unknown invariant"<<std::endl;
			       break;
			   case 2:
			       res /= mcf.s(s[i[1]-1].first, s[i[1]-1].second);
			       break;
			   case 3:
			       res /= (mcf.s(s[i[1]-1].first, s[i[1]-1].second) - mcf.s(s[i[2]-1].first, s[i[2]-1].second));
			       break;
			   default:
			       std::cout<<"WARNING: asked for unknown function of invariant"<<std::endl;
			   }
		       return res;
		   };
	} else {
	    std::cout<<"WARNING: a monomial list is too long"<<std::endl;
	}
	toreturn.push_back({extract_String_content(tmp[0]), fRat});
    }

    return toreturn;
}

template <typename T, typename FType>
std::pair<std::string, std::vector<std::pair<std::string, std::function<T(const momentumD_configuration<T, 4>&,const bool)>>>>
MapToBasis<T, FType>::extract_replacement_rule(const std::string& rule) {
    MathList mlist(rule, "Rule");

    std::pair<std::string, std::vector<std::pair<std::string, std::function<T(const momentumD_configuration<T, 4>&,const bool)>>>> toreturn;
    toreturn.first = extract_String_content(mlist.tail[0]);
    toreturn.second = extract_replacing_lc(MathList(mlist.tail[1], "List"));

    auto tail = mlist[1];

    return toreturn;
}

template <typename T, typename FType> std::string MapToBasis<T, FType>::get_filename(const PartialAmplitudeInput& input) {

    std::string filename = DATADIR;

    if (input.get_multiplicity() == 4) {
        filename += "/2L_gggg_replacements.txt";
        return filename;
    }
    else if (input.get_multiplicity() == 5) {
        if (settings::integrals::integral_family_2L.value == settings::integrals::family_types_2l::goncharovs) { filename += "/2L_ggggg_replacements.txt"; }
        else if (settings::integrals::integral_family_2L.value == settings::integrals::family_types_2l::pentagon_functions) {
            filename += "/2L_ggggg_pentagon_replacements.txt";
        }
	else if (settings::integrals::integral_family_2L.value == settings::integrals::family_types_2l::pentagon_functions_new) {
	    if (input.get_number_of_photons()) {
		if (input.get_nf_powers())
		    filename += "/qqyyy_nf1_pentagon_new_replacements.txt";
		else
		    filename += "/qqyyy_pentagon_new_replacements.txt";
            }
            else {
                throw std::runtime_error("No file for replacements for the provided integral family.");
            }
        }
        else {
            throw std::runtime_error("No file for replacements for the provided integral family.");
        }
        return filename;
    }
    else {
        return "";
    }
}

// ----------------------------------------------------------------------------
// ----------------- External declarations ------------------------------------
// ----------------------------------------------------------------------------
#define EXTERN_INSTANTIATE_MAP_TO_BASIS(T, OUTF, INF) extern template class MapToBasis<T, StdBasisFunction<OUTF, INF>>;

EXTERN_INSTANTIATE_MAP_TO_BASIS(F32, C, R)
#ifdef HIGH_PRECISION
EXTERN_INSTANTIATE_MAP_TO_BASIS(F32, CHP, RHP)
#endif
#ifdef VERY_HIGH_PRECISION
EXTERN_INSTANTIATE_MAP_TO_BASIS(F32, CVHP, RVHP)
#endif

} // namespace Integrals
} // namespace Caravel
