#pragma once

#include <functional>
#include <utility>
#include <string>
#include <vector>
#include <iostream>
#include <cstdlib>

#include "IntegralLibrary/FunctionBasisExpansion.h"
#include "IntegralLibrary/InputVariables.h"

#include "Core/typedefs.h"

#include "wise_enum.h"

namespace Caravel {
namespace Integrals {

/// Identifier for the signs of the invariants.
WISE_ENUM_CLASS(InvariantSigns,
    /// Positive invariant (s-channel-like)
    positive,
    /// Negative invariant (t-channel-like)
    negative,
    /// Any possible sign
    anything,
    /// Unknown
    undefined
)

bool compare(const InvariantSigns&,const InvariantSigns&);
bool compare(const std::vector<InvariantSigns>&,const std::vector<InvariantSigns>&);

template <typename COUT, typename CIN, typename FOUT, typename FIN>
class MasterIntegralPointer {
  private:
    using FBE = FunctionBasisExpansion<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<FOUT, FIN>>;
    using RegionSelector = std::function<FBE(const std::vector<InvariantSigns>&, const std::vector<std::string>&)>;

  public:
    MasterIntegralPointer() = default;
    MasterIntegralPointer(const RegionSelector& f, const std::vector<InputVariable>& invs) : region_selector{f}, input_variables{invs} {};
    auto operator()(const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) const -> FBE;
    auto get_variables() const -> std::vector<InputVariable>;

  private:
    RegionSelector region_selector;
    std::vector<InputVariable> input_variables;
};

} // namespace Integrals
} // namespace Caravel

#include "MasterIntegralPointer.hpp"
