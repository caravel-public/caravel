#pragma once

#include <tuple>
#include <unordered_map>
#include <utility>
#include <string>

#include "Graph/GraphKin.h"
#include "IntegralLibrary/MasterIntegralPointer.h"
#include "IntegralLibrary/MasterIntegralInputGenerator.h"
#include "Graph/IntegralGraph.h"

#include "Core/typedefs.h"

namespace Caravel {
namespace Integrals {

/// The type which is the generic topology indentifier
using TopoIDType = lGraph::Graph<lGraph::Node,lGraph::Connection<lGraph::Strand>>;

#define _add_input_generator(T)\
    MasterIntegralInputGenerator<T>

#define _add_integral(COUT, CIN, FOUT, FIN)\
    MasterIntegralPointer<COUT, CIN, FOUT, FIN>

/// TODO: Still need to add the function that evaluates the input
using MasterIntegralData = 
std::tuple<
lGraph::xGraph,
_add_integral(C,R,C,R),
_add_integral(C,C,C,C),
_add_input_generator(R),
_add_input_generator(C)
#ifdef USE_FINITE_FIELDS
  , _add_integral(F32,F32,C,R)
  , _add_integral(F32,F32,C,C)
  , _add_input_generator(F32)
#endif
#ifdef HIGH_PRECISION
  , _add_integral(CHP,RHP,CHP,RHP)
  , _add_integral(CHP,CHP,CHP,CHP)
  , _add_input_generator(RHP)
  , _add_input_generator(CHP)
#ifdef USE_FINITE_FIELDS
  , _add_integral(F32,F32,CHP,RHP)
  , _add_integral(F32,F32,CHP,CHP)
#endif
#endif
#ifdef VERY_HIGH_PRECISION
  , _add_integral(CVHP,RVHP,CVHP,RVHP)
  , _add_integral(CVHP,CVHP,CVHP,CVHP)
  , _add_input_generator(RVHP)
  , _add_input_generator(CVHP)
#ifdef USE_FINITE_FIELDS
  , _add_integral(F32,F32,CVHP,RVHP)
  , _add_integral(F32,F32,CVHP,CVHP)
#endif
#endif
>;

#undef _add_type

/// A global container for loading master terms
extern std::unordered_map<lGraph::IntegralGraph,MasterIntegralData> master_integral_data_holder;

} // namespace Integrals
} // namespace Caravel
