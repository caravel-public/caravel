
namespace Caravel {
namespace Integrals {

// ----------------------------------------------------------------------------
// -------------------------- MasterIntegral ----------------------------------
// ----------------------------------------------------------------------------

template <typename CType, typename FType>
MasterIntegral<CType, FType>::MasterIntegral(const std::vector<std::pair<FType, CType>>& basis_elems) {
    // Fill the basis with the provided information
    for (auto elem : basis_elems) { abstract_basis.add_basis_element(elem.first, elem.second); }
}

template <typename CType, typename FType>
unsigned long MasterIntegral<CType, FType>::n_basis_funcs() const {
    return abstract_basis.n_basis_funcs();
}

template <typename CType, typename FType>
bool MasterIntegral<CType, FType>::contains_basis_function(const FType& basis_func) const {
    return abstract_basis.contains(basis_func);
}

template <typename CType, typename FType>
void MasterIntegral<CType, FType>::add_basis_element(const FType& basis_func, const CType& coeff) {
    abstract_basis.add_basis_element(basis_func, coeff);
}

// The return type is given by the return type of the coefficient.
template <typename CType, typename FType>
NumericFunctionBasisExpansion<typename CType::output_type, FType>
MasterIntegral<CType, FType>::eval_basis_coeffs(const std::vector<typename CType::input_type>& args) {
    return abstract_basis.eval_coeffs(args);
}

template <typename CType, typename FType>
template <typename TT>
typename std::enable_if<!is_exact<TT>::value &&
                            std::is_same<typename FType::input_type, typename CType::input_type>::value,
                        typename CType::output_type>::type // Series<typename FType::output_type>
MasterIntegral<CType, FType>::compute(const std::vector<typename FType::input_type>& args) {

    auto res = eval_basis_coeffs(args);
    // Set the toreturn value to the first coefficient, times the first basis function.
    // We cannot first set a zero value because we need to know the epsilon powers.
    auto function = res.begin()->first;
    auto toreturn = res.begin()->second * function(args); // * basis_functions.at(res.begin()->first)(args);

    // Now from the second basis function on, add the others to the result.
    for (auto it = ++res.begin(); it != res.end(); ++it) {
        // Need to first copy the basis function into function because keys cannot be modified!
        function = it->first;
        toreturn = toreturn + it->second * function(args);
    }

    return toreturn;
}

template <typename CType, typename FType>
FunctionBasisExpansion<CType, FType> MasterIntegral<CType, FType>::get_basis() const{
    return abstract_basis;
}

// ----------------------------------------------------------------------------
// ------------ Declare external instantiations -------------------------------
// ----------------------------------------------------------------------------
#define EXTERN_INSTANTIATE_MI(OUT, IN)                                                                                 \
    extern template class MasterIntegral<FBCoefficient<Series<OUT>, IN>, StdBasisFunction<OUT, IN>>;

EXTERN_INSTANTIATE_MI(C, R)
#ifdef HIGH_PRECISION
EXTERN_INSTANTIATE_MI(CHP, RHP)
#endif
#ifdef VERY_HIGH_PRECISION
EXTERN_INSTANTIATE_MI(CVHP, RVHP)
#endif

#ifdef USE_FINITE_FIELDS
#define EXTERN_INSTANTIATE_MI_FF(COUT, CIN, OUT, IN)                                                                                 \
    extern template class MasterIntegral<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<OUT, IN>>;

EXTERN_INSTANTIATE_MI_FF(F32, F32, C, R)
#ifdef HIGH_PRECISION
EXTERN_INSTANTIATE_MI_FF(F32, F32, CHP, RHP)
#endif
#ifdef VERY_HIGH_PRECISION
EXTERN_INSTANTIATE_MI_FF(F32, F32, CVHP, RVHP)
#endif
#endif

} // namespace Integrals
} // namespace Caravel
