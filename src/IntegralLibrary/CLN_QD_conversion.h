#ifndef _CLN_QD_CONVERSION_H_
#define _CLN_QD_CONVERSION_H_

#include <string>
#include <sstream>
#include <utility>
#include <algorithm>
#include <stdexcept>
#include <type_traits>
#include <complex>

#include "Core/typedefs.h"
#include "Core/type_traits_extra.h"
#if defined(HIGH_PRECISION) || defined(VERY_HIGH_PRECISION)
#include "qd/qd_real.h"
#endif

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-local-typedefs"
#include "cln/complex.h"
#include "cln/rational.h"
#include "cln/real.h"
#include "cln/integer.h"
#include "cln/sfloat.h"
#include "cln/ffloat.h"
#include "cln/dfloat.h"
#include "cln/lfloat.h"

#include "cln/complex_io.h"
#include "cln/rational_io.h"
#include "cln/integer_io.h"
#include "cln/sfloat_io.h"
#include "cln/ffloat_io.h"
#include "cln/dfloat_io.h"
#include "cln/lfloat_io.h"
#pragma GCC diagnostic pop

namespace Caravel {
namespace Integrals {

// ----- Hide implementation. -----

namespace _qd_cln {

/// Convert a cl_R or cl_LF to string
template <typename CLN>
std::string cln_to_string(const CLN& cln){
    std::ostringstream oss;
    // Set maximal precision.
    oss.precision(66);
    oss << cln;
    std::string s = oss.str();
    std::replace( s.begin(), s.end(), 'L', 'e');
    return s;
}

/// Convert QD real to CLN real.
template <typename QD>
cln::cl_LF _qd_to_cln_real(const QD& qd);

/// Convert R to cl_LF.
template <>
inline cln::cl_LF _qd_to_cln_real(const R& qd) {
    std::ostringstream oss;
    oss.precision(19);
    oss << qd;
    // Check whether we have an integer or not
    if (oss.str().find('.') != std::string::npos){
        return cln::cl_LF((oss.str() + "_18").c_str());
    }
    else {
        return cln::cl_LF((oss.str()+".0_18").c_str());
    }
}

/// Convert RHP to cl_LF.
#ifdef HIGH_PRECISION
template <>
inline cln::cl_LF _qd_to_cln_real(const RHP& qd) {
    if (qd.to_string().find('.') != std::string::npos){
        return cln::cl_LF((qd.to_string() + "_34").c_str());
    }
    else {
        return cln::cl_LF((qd.to_string() + "._34").c_str());
    }
}
#endif

/// Convert RVHP to cl_LF.
#ifdef VERY_HIGH_PRECISION
template <>
inline cln::cl_LF _qd_to_cln_real(const RVHP& qd) {
    if (qd.to_string().find('.') != std::string::npos){
        return cln::cl_LF((qd.to_string() + "_66").c_str());
    }
    else {
        return cln::cl_LF((qd.to_string() + "._66").c_str());
    }
}
#endif

/// Convert cl_LF to QD.
template <typename QD>
QD _cln_to_qd_real(const cln::cl_LF& cln);

/// Convert cl_LF to R.
template <>
inline R _cln_to_qd_real(const cln::cl_LF& cln) {
    return cln::double_approx(cln);
}

/// Convert cl_LF to RHP.
#ifdef HIGH_PRECISION
template <>
inline RHP _cln_to_qd_real(const cln::cl_LF& cln) {
    std::ostringstream oss;
    oss << cln;
    std::string s = oss.str();
    std::replace( s.begin(), s.end(), 'L', 'e');
    return RHP(s.c_str());
}
#endif

#ifdef VERY_HIGH_PRECISION
template <>
inline RVHP _cln_to_qd_real(const cln::cl_LF& cln) {
    std::ostringstream oss;
    oss << cln;
    std::string s = oss.str();
    std::replace( s.begin(), s.end(), 'L', 'e');
    return RVHP(s.c_str());
}
#endif


/// Convert a complex QD type to a cl_N.
template <typename QD>
cln::cl_N _qd_to_cln_complex(const QD& qd);

/// Convert C to cl_N.
template <>
inline cln::cl_N _qd_to_cln_complex(const C& qd) {
    return cln::complex(_qd_to_cln_real(qd.real()), _qd_to_cln_real(qd.imag()));
}

/// Convert CHP to cl_N.
#ifdef HIGH_PRECISION
template <>
inline cln::cl_N _qd_to_cln_complex(const CHP& qd) {
    return cln::complex(_qd_to_cln_real(qd.real()), _qd_to_cln_real(qd.imag()));
}
#endif

/// Convert CVHP to cl_N.
#ifdef VERY_HIGH_PRECISION
template <>
inline cln::cl_N _qd_to_cln_complex(const CVHP& qd) {
    return cln::complex(_qd_to_cln_real(qd.real()), _qd_to_cln_real(qd.imag()));
    //return cln::cl_N((qd.real().to_string() + "+" + qd.imag().to_string() + "i").c_str());
}
#endif

/// Convert complex CLN to complex QD.
template <typename QD>
QD _cln_to_qd_complex(const cln::cl_N& cln);

/// Convert cl_N to C.
template <>
inline C _cln_to_qd_complex(const cln::cl_N& cln) {
    return C(cln::double_approx(cln::realpart(cln)), cln::double_approx(cln::imagpart(cln)));
}

/// Convert cl_N to CHP.
#ifdef HIGH_PRECISION
template <>
inline CHP _cln_to_qd_complex(const cln::cl_N& cln) {
    return CHP(cln_to_string(cln::realpart(cln)).c_str(), cln_to_string(cln::imagpart(cln)).c_str());
}
#endif

/// Convert cl_N to CVHP.
#ifdef VERY_HIGH_PRECISION
template <>
inline CVHP _cln_to_qd_complex(const cln::cl_N& cln) {
    return CVHP(cln_to_string(cln::realpart(cln)).c_str(), cln_to_string(cln::imagpart(cln)).c_str());
}
#endif

/// Conversion from QD to CLN
template <typename CLN, typename QD, typename IsComplex>
struct _qd_to_cln {};

/// Conversion from complex QD to complex CLN.
template <typename CLN, typename QD>
struct _qd_to_cln<CLN, QD, std::true_type> {
    CLN convert(const QD& qd) { return _qd_to_cln_complex(qd); }
};

/// Conversion from real QD to cl_LF.
template <typename CLN, typename QD>
struct _qd_to_cln<CLN, QD, std::false_type> {
    CLN convert(const QD& qd) { return _qd_to_cln_real(qd); }
};

/// Conversion from CLN to QD.
template <typename QD, typename CLN, typename IsComplex>
struct _cln_to_qd {};

/// Conversion from complex CLN to complex QD.
template <typename QD, typename CLN>
struct _cln_to_qd<QD, CLN, std::true_type> {
    QD convert(const CLN& cln) { return _cln_to_qd_complex<QD>(cln); }
};

/// Conversion from real CLN to real QD
template <typename QD, typename CLN>
struct _cln_to_qd<QD, CLN, std::false_type> {
    QD convert(const CLN& cln) { return _cln_to_qd_real<QD>(cln); }
};

} // namespace _qd_cln

// ----- Expose the final interface. -----

/**
 * Convert a complex QD number to a complex cln::cl_N type.
 *
 * Notice that QD collectively stands for C, CHP and CVHP (hence it also
 * includes simple std::complex<double> = C).
 *
 * @param  qd The complex QD number to be converted to cl_N.
 * @return    The cl_N type number holding the value of the input QD number.
 */
template <typename QD>
typename std::enable_if<is_complex<QD>::value, cln::cl_N>::type qd_to_cln(const QD& qd){
    return _qd_cln::_qd_to_cln<cln::cl_N, QD, std::true_type>().convert(qd);
}

/**
 * Convert a real QD number to a real cln::cl_LF type.
 *
 * Notice that QD collectively stands for R, RHP and RVHP (hence it also
 * includes simple double = R).
 *
 * @param  qd The real QD number to be converted to cl_LF.
 * @return    The cl_LF type number holding the value of the input QD number.
 */
template <typename QD>
typename std::enable_if<!is_complex<QD>::value, cln::cl_LF>::type qd_to_cln(const QD& qd){
    return _qd_cln::_qd_to_cln<cln::cl_LF, QD, std::false_type>().convert(qd);
}

/**
 * Convert a cl_N number to a complex QD number.
 *
 * Notice that QD collectively stands for C, CHP and CVHP (hence it also
 * includes simple std::complex<double> = C).
 *
 * @param  cln The cl_N number to be converted to a complex QD type.
 * @return     The complex QD number holding the value of the input cl_N number.
 */
template <typename QD>
QD cln_to_qd(const cln::cl_N& cln){
    return _qd_cln::_cln_to_qd<QD, cln::cl_N, std::true_type>().convert(cln);
}

/**
 * Convert a cl_LF number to a real QD number.
 * Notice that QD collectively stands for R, RHP and RVHP (hence it also
 * includes simple double = R).
 *
 * @param  cln The cl_LF number to be converted to a real QD number.
 * @return     The real QD number holding the value of the input cl_LF number.
 */
template <typename QD>
QD cln_to_qd(const cln::cl_LF& cln){
    return _qd_cln::_cln_to_qd<QD, cln::cl_LF, std::false_type>().convert(cln);
}

} // namespace Integrals
} // namespace Caravel

#endif // _CLN_QD_CONVERSION_H_
