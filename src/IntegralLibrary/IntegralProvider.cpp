#include "IntegralProvider.h"

namespace Caravel {
namespace Integrals {

using TopoIDType = lGraph::Graph<lGraph::Node,lGraph::Connection<lGraph::Strand>>;

void read_permutation_of_custom_basis_from_stream(std::ifstream& custom, std::unordered_map<TopoIDType, std::vector<size_t>>& map){
    std::string thegraph;
    while (std::getline(custom, thegraph)) {
        if (thegraph.at(0) == '#') continue;
        std::string therelabeling;
        if (std::getline(custom, therelabeling)) {
            TopoIDType graph(thegraph.c_str());
            std::vector<size_t> permutation;
            MathList mrelabel(therelabeling, "GraphRelabel");
            MathList ext_permutation(mrelabel[1], "ExternalPermutation");
            for (auto& index : ext_permutation) {
                auto ii = std::stoul(index);
                if (ii == 0) {
                    std::cerr << "ERROR: permutation labels in ExternalPermutation[] MathList are 1-based! Found a 0" << std::endl;
                    std::exit(1);
                }
                permutation.push_back(ii - 1);
            }

            map[graph] = permutation;
        }
        else {
            std::cerr << "ERROR: reading custom basis got no ExternalPermutation[] for the graph: " << thegraph << std::endl;
            std::exit(1);
        }
    }
}

_INST_INTEGRALPROVIDER(, C, R, C, R)
_INST_INTEGRALPROVIDER(, C, C, C, C)

#ifdef USE_FINITE_FIELDS
_INST_INTEGRALPROVIDER(, F32, F32, C, R)
_INST_INTEGRALPROVIDER(, F32, F32, C, C)
#endif

#ifdef HIGH_PRECISION
_INST_INTEGRALPROVIDER(, CHP, RHP, CHP, RHP)
_INST_INTEGRALPROVIDER(, CHP, CHP, CHP, CHP)

#ifdef USE_FINITE_FIELDS
_INST_INTEGRALPROVIDER(, F32, F32, CHP, RHP)
_INST_INTEGRALPROVIDER(, F32, F32, CHP, CHP)
#endif
#endif

#ifdef VERY_HIGH_PRECISION
_INST_INTEGRALPROVIDER(, CVHP, RVHP, CVHP, RVHP)
_INST_INTEGRALPROVIDER(, CVHP, CVHP, CVHP, CVHP)

#ifdef USE_FINITE_FIELDS
_INST_INTEGRALPROVIDER(, F32, F32, CVHP, RVHP)
_INST_INTEGRALPROVIDER(, F32, F32, CVHP, CVHP)
#endif
#endif


}
}
