#pragma once

#include <iostream>
#include <string>
#include <vector>

#include "wise_enum.h"

namespace Caravel {
namespace Integrals {

/**
 * Categorize the possible input variables types for master integrals.
 *
 * So far they can be Mandelstams, Twistors, masses or Tr5. This can be extended in the future.
 * 
 * Twistor5 indicates that the 5 point twistor parameterization is to be used.
 */
WISE_ENUM_CLASS(InvariantType, Mandelstam, Twistor5, Mass, Mass2, Tr5, SignTr5)

/**
 * Input variables for the integrals.
 *
 * The input variables are characterized by their type such as Mandelstam, Twistor, Mass, etc as specified
 * by the public member `invariant_type`. The invariant can be constructed from several momenta whose indices
 * are stored in the vector `momentum_indices`.
 */
struct InputVariable {
    /**
     * Construct an input variable by providing its type and which momenta it is constructed from.
     * 
     * @param type      The type of the invariant, e.g. Mandelstam, Twistor, Mass. See the WISE_ENUM_CLASS `InvariantType`.
     * @param indices   The indices of the momenta that the invariant is constructed from.
     */
    InputVariable(const InvariantType& type, const std::vector<std::size_t>& indices) : invariant_type{type}, momentum_indices{indices} {}
    /// Get a string representation of the invariant.
    std::string as_string() const;
    /// The type of the invariant.
    InvariantType invariant_type;
    /// Storage for the indices of the momenta that make up the invariant.
    std::vector<std::size_t> momentum_indices;
    
};


/**
 * Make an invariant printable.
 * 
 * @param os    The output stream that the invariant shall be printed to.
 * @param iv    The invariant that shall be printed to the output stream.
 * @return      The passed stream that now contains the invariant to be printed.
 */
std::ostream& operator<<(std::ostream& os, const InputVariable& iv);

} // namespace Integrals
} // namespace Caravel