#include "MasterIntegral.h"

namespace Caravel {
namespace Integrals {

// ----------------------------------------------------------------------------
// ----------------------------- Instantiations -------------------------------
// ----------------------------------------------------------------------------

#define INSTANTIATE_MI(OUT, IN)                                                                                        \
    template class MasterIntegral<FBCoefficient<Series<OUT>, IN>, StdBasisFunction<OUT, IN>>;

INSTANTIATE_MI(C, R)
#ifdef HIGH_PRECISION
INSTANTIATE_MI(CHP, RHP)
#endif
#ifdef VERY_HIGH_PRECISION
INSTANTIATE_MI(CVHP, RVHP)
#endif

#ifdef USE_FINITE_FIELDS
#define INSTANTIATE_MI_FF(COUT, CIN, OUT, IN)                                                                                        \
    template class MasterIntegral<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<OUT, IN>>;

INSTANTIATE_MI_FF(F32, F32, C, R)
#ifdef HIGH_PRECISION
INSTANTIATE_MI_FF(F32, F32, CHP, RHP)
#endif
#ifdef VERY_HIGH_PRECISION
INSTANTIATE_MI_FF(F32, F32, CVHP, RVHP)
#endif
#endif

} // namespace Integrals
} // namespace Caravel
