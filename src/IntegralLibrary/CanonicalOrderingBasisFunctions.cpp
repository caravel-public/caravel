#include "CanonicalOrderingBasisFunctions.h"

namespace Caravel {
namespace Integrals {

std::string concatenate(const std::vector<std::string>& factors, char sep) {
    std::ostringstream oss;
    for (size_t i = 0; i < factors.size(); ++i) {
        if (i != factors.size() - 1) {
            oss << factors[i] << sep;
        } else {
            oss << factors[i];
        }
    }
    return oss.str();
}

bool is_goncharov(const std::string& function) {
    if (std::string(function.begin(), function.begin() + 6) == "G<Out>")
        return true;
    else
        return false;
}

std::string clean_goncharov_args(const std::string& function) {

    // If the function is not a Goncharov, do nothing.
    if (!is_goncharov(function)) return function;

    // If it is a goncharov, extract its arguments
    std::string argstring(function.begin() + 7, function.end() - 1);

    std::vector<std::string> args;
    std::ostringstream oss;

    for (const auto& elem : argstring) {

        // Ignore whitespaces
        if(elem == ' ') continue;

        if (elem == ',') {
            args.push_back(oss.str());
            oss.str(std::string());
            continue;
        } else {
            oss << elem;
        }
    }
    args.push_back(oss.str());

    for (size_t i = 0; i < args.size(); ++i) {
        std::ostringstream sstream;
        std::vector<std::string> terms;

        for (const auto& elem : args[i]) {
            if (elem != '+') {
                sstream << elem;
            } else {
                terms.push_back(sstream.str());
                sstream.str(std::string());
            }
        }
        terms.push_back(sstream.str());

        // terms = {"s23*T(-1)", "s12*T(-1)"};
        std::sort(terms.begin(), terms.end());

        args[i] = concatenate(terms, '+');
    }

    std::string only_args = concatenate(args, ',');
    return "G<Out>(" + only_args + ")";
}

std::string canonical_ordering_hash_string(const std::string& hashstring) {

    // Count brackets, open brackets count as plus one, closed brackets as
    // minus one.
    int bracket_count = 0;

    // Vector that will contain the different factors of the product of basis
    // functions.
    std::vector<std::string> factors = {};

    // Stringstream which will contain the canonically ordered product of
    // factors in the basis function product.
    std::ostringstream oss;

    // Loop through all of the characters in the string
    for (const auto& elem : hashstring) {

        // Count open and closed brackets
        if (elem == '(' || elem == '<' || elem == '[') { ++bracket_count; }
        if (elem == ')' || elem == '>' || elem == ']') { --bracket_count; }

        // Extract the different factors in the product of basis functions
        // and add them to the vector 'factors'.
        if (elem != '*') {
            oss << elem;
        } else if (elem == '*' && bracket_count != 0) {
            oss << elem;
        } else {
            // Fill the factor in the vector
            factors.push_back(oss.str());
            // Empty the stringstream
            oss.str(std::string());
        }
    }

    factors.push_back(oss.str());

    // Clean the arguments of the Goncharov's
    // TODO: Only works if the arguments are single invariants or sums thereof
    for(size_t i = 0; i < factors.size(); ++i){
        factors[i] = clean_goncharov_args(factors[i]);
    }

    // Write powers as products
    std::vector<std::string> new_factors;
    for (auto& factor : factors){
      if (factor.substr(0,8) == "prodpow("){
        auto base = factor.substr(8, factor.length()-8-3);
        auto power = std::stoi(factor.substr(factor.length()-2,1));
        for (int i = 0; i < power; i++){
          new_factors.push_back(base);
        }
      }
      else{
        new_factors.push_back(factor);
      }
    }

    auto find_n_replace_all = [](std::string& in, const std::string& target, const std::string& repl){
        int pos = in.find(target);
        auto len = target.size();
        while (pos != -1) {
            in.replace(pos, len, repl);
            pos = in.find(target);
        }
    };

    for (auto& factor: new_factors){
        find_n_replace_all(factor, "<Out>", "");
        find_n_replace_all(factor, "(", "[");
        find_n_replace_all(factor, ")", "]");
        find_n_replace_all(factor, "zeta", "Zeta");
	find_n_replace_all(factor, "\\\"", "\"");
    }


    // Sort the different factors in the vector to obtain a canonical
    // ordering.
    std::sort(new_factors.begin(), new_factors.end());

    // Concatenate the newly ordered factors to a new hashstring.
    auto canonical= concatenate(new_factors, '*');


    return canonical;
}

} // namespace Integrals
} // namespace Caravel
