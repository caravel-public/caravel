#include "FunctionBasisExpansion.h"

namespace Caravel {
namespace Integrals {
// ----------------------------------------------------------------------------
// ----------------------------- Instantiations -------------------------------
// ----------------------------------------------------------------------------
#ifdef USE_FINITE_FIELDS
#define INSTANTIATE_FBE_FF(CIN, COUT, OUT, IN)                                                                                \
    template class FunctionBasisExpansion<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<OUT, IN>>;

    INSTANTIATE_FBE_FF(BigRat, BigRat, C, R)
#ifdef HIGH_PRECISION
    INSTANTIATE_FBE_FF(BigRat, BigRat, CHP, RHP)
#endif
#ifdef VERY_HIGH_PRECISION
    INSTANTIATE_FBE_FF(BigRat, BigRat, CVHP, RVHP)
#endif
#endif

#ifdef USE_FINITE_FIELDS
#define INSTANTIATE_NFBE_FF(COUT, OUT, IN)                                                                                      \
    template class NumericFunctionBasisExpansion<Series<COUT>, StdBasisFunction<OUT, IN>>;                              \
                                                                                                                       \
    template NumericFunctionBasisExpansion<Series<COUT>, StdBasisFunction<OUT, IN>> operator+(                          \
        const NumericFunctionBasisExpansion<Series<COUT>, StdBasisFunction<OUT, IN>>& b1,                               \
        const NumericFunctionBasisExpansion<Series<COUT>, StdBasisFunction<OUT, IN>>& b2);                              \
                                                                                                                       \
    template NumericFunctionBasisExpansion<Series<COUT>, StdBasisFunction<OUT, IN>> operator-(                          \
        const NumericFunctionBasisExpansion<Series<COUT>, StdBasisFunction<OUT, IN>>& b1,                               \
        const NumericFunctionBasisExpansion<Series<COUT>, StdBasisFunction<OUT, IN>>& b2);

INSTANTIATE_NFBE_FF(BigRat, C, R)
#ifdef HIGH_PRECISION
INSTANTIATE_NFBE_FF(BigRat, CHP, RHP)
#endif
#ifdef VERY_HIGH_PRECISION
INSTANTIATE_NFBE_FF(BigRat, CVHP, RVHP)
#endif
#endif

#ifdef USE_FINITE_FIELDS
#define INSTANTIATE_OPERATOR_SCALAR_FF(COUT, OUT, IN, TYPE)                                                              \
    template NumericFunctionBasisExpansion<Series<COUT>, StdBasisFunction<OUT, IN>> operator*(                   \
        const TYPE& scal, const NumericFunctionBasisExpansion<Series<COUT>, StdBasisFunction<OUT, IN>>& basis);         \
                                                                                                                       \
    template NumericFunctionBasisExpansion<Series<COUT>, StdBasisFunction<OUT, IN>> operator*<>(                 \
        const NumericFunctionBasisExpansion<Series<COUT>, StdBasisFunction<OUT, IN>>& basis, const TYPE& scal);         \
                                                                                                                       \
    template NumericFunctionBasisExpansion<Series<COUT>, StdBasisFunction<OUT, IN>> operator/                    \
        <>(const NumericFunctionBasisExpansion<Series<COUT>, StdBasisFunction<OUT, IN>>& basis, const TYPE& scal);

INSTANTIATE_OPERATOR_SCALAR_FF(BigRat, C, R, BigRat)
#ifdef HIGH_PRECISION
INSTANTIATE_OPERATOR_SCALAR_FF(BigRat, CHP, RHP, BigRat)
#endif
#ifdef VERY_HIGH_PRECISION
INSTANTIATE_OPERATOR_SCALAR_FF(BigRat, CVHP, RVHP, BigRat)
#endif
#endif

} // namespace Integrals
} // namespace Caravel
