#pragma once

#include <functional>
#include <vector>

#include "Core/momD.h"
#include "Core/momD_conf.h"

#include "Core/typedefs.h"

#ifdef HIGH_PRECISION
#include "qd/dd_real.h"
#endif
#ifdef VERY_HIGH_PRECISION
#include "qd/qd_real.h"
#endif

namespace Caravel {
namespace Integrals {

/**
 * For a given momentum configuration evaluate the input for the master integral.
 */
template <typename T> class MasterIntegralInputGenerator {
  private:
    // Convenience typedef.
    using InputGenerator = std::function<std::vector<T>(const momD_conf<T, 4>&)>;

  public:
    MasterIntegralInputGenerator() = default;
    /// Construct a MasterIntegralInputGenerator by providing a function to transform 
    /// a momentum configuration into a vector of invariants.
    MasterIntegralInputGenerator(const InputGenerator& generator) : input_generator{generator} {}
    /// Call the function that transforms the momentum configuration into a vector of invariants.
    std::vector<T> operator()(const momD_conf<T, 4>& mc) const { return input_generator(mc); }

  private:
    /// The function that transforms a momentum configuration into a vector of invariants.
    InputGenerator input_generator;
};

#define EXTERN_INSTANTIATE_MASTER_INTEGRAL_INPUT_GENERATOR(T) extern template class MasterIntegralInputGenerator<T>;

EXTERN_INSTANTIATE_MASTER_INTEGRAL_INPUT_GENERATOR(R)
EXTERN_INSTANTIATE_MASTER_INTEGRAL_INPUT_GENERATOR(C)

#ifdef USE_FINITE_FIELDS
EXTERN_INSTANTIATE_MASTER_INTEGRAL_INPUT_GENERATOR(F32)
#endif

#ifdef HIGH_PRECISION
EXTERN_INSTANTIATE_MASTER_INTEGRAL_INPUT_GENERATOR(RHP)
EXTERN_INSTANTIATE_MASTER_INTEGRAL_INPUT_GENERATOR(CHP)
#endif

#ifdef VERY_HIGH_PRECISION
EXTERN_INSTANTIATE_MASTER_INTEGRAL_INPUT_GENERATOR(RVHP)
EXTERN_INSTANTIATE_MASTER_INTEGRAL_INPUT_GENERATOR(CVHP)
#endif

} // namespace Integrals
} // namespace Caravel
