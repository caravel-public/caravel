#include <stdexcept>

namespace Caravel {
namespace Integrals {

template <typename COUT, typename CIN, typename FOUT, typename FIN>
auto MasterIntegralPointer<COUT, CIN, FOUT, FIN>::operator()(const std::vector<InvariantSigns>& signs, const std::vector<std::string>& names) const -> FBE {
    return region_selector(signs, names);
}

template <typename COUT, typename CIN, typename FOUT, typename FIN>
auto MasterIntegralPointer<COUT, CIN, FOUT, FIN>::get_variables() const -> std::vector<InputVariable> {
    return input_variables;
}

} // namespace Integrals
} // namespace Caravel
