
namespace Caravel {
namespace Integrals {
// ----------------------------------------------------------------------------
// -------------------------- FBCoefficient -----------------------------------
// ----------------------------------------------------------------------------

template <typename Out, typename In>
FBCoefficient<Out, In>::FBCoefficient(const std::function<Out(const std::vector<In>&)>& f) : func{f}, num_result{Out{}} {}

template <typename Out, typename In>
FBCoefficient<Out, In>::FBCoefficient(const FBCoefficient<Out, In>& c) = default;

template <typename Out, typename In>
FBCoefficient<Out, In>& FBCoefficient<Out, In>::operator=(const FBCoefficient<Out, In>& c) = default;

template <typename Out, typename In>
Out FBCoefficient<Out, In>::compute(const std::vector<In>& args) {
    is_evaluated = true;
    num_result = func(args);
    return num_result;
}

template <typename Out, typename In>
Out FBCoefficient<Out, In>::get_value() const {
    if (!is_evaluated) { throw std::runtime_error("Coefficient has not been evaluated. Cannot return value!"); }
    return num_result;
}

// ----------------------------------------------------------------------------
// ------------------------- EXPLICIT INSTANTIATION ---------------------------
// ----------------------------------------------------------------------------
#define EXTERN_INSTANTIATE_COEFF(OUT, IN) \
extern template class FBCoefficient<OUT, IN>;

EXTERN_INSTANTIATE_COEFF(Series<C>, R)
#ifdef HIGH_PRECISION
EXTERN_INSTANTIATE_COEFF(Series<CHP>, RHP)
#endif
#ifdef VERY_HIGH_PRECISION
EXTERN_INSTANTIATE_COEFF(Series<CVHP>, RVHP)
#endif

#ifdef USE_FINITE_FIELDS
EXTERN_INSTANTIATE_COEFF(Series<F32>, F32)
#endif

} // namespace Integrals
} // namespace Caravel
