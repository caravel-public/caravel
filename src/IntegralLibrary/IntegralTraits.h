#ifndef _INTEGRALTRAITS_H_
#define _INTEGRALTRAITS_H_

#include <type_traits>

namespace Caravel {
namespace Integrals {

/**
 * Access to the input and output types.
 *
 * Both the basis functions and coefficients of Master Integrals
 * inherit from this struct and thus define input and output types.
 */
template <typename In, typename Out>
struct InOutTrait {
    /// The input type of the object (usually real kinematic invariants).
    typedef In input_type;
    /// The output type of the object.
    typedef Out output_type;
};

/// Trait to check whether a type is a StdBasisFunction.
template <typename F>
struct is_std_basis_function : public std::false_type {};

// Forward declaration
template <typename Out, typename In>
class StdBasisFunction;

/// Trait to check whether a type is a StdBasisFunction.
///
/// Specialization for StdBasisFunction!
template <typename Out, typename In>
struct is_std_basis_function<Integrals::StdBasisFunction<Out, In>> : public std::true_type {};

} // namespace Integrals
} // namespace Caravel

#endif // _INTEGRALTRAITS_H_
