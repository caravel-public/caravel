#ifndef _MASTERINTEGRAL_H_
#define _MASTERINTEGRAL_H_

#include <type_traits>

#include "Core/type_traits_extra.h"
#include "FBCoefficient.h"
#include "FunctionBasisExpansion.h"
#include "BasisElementFunction.h"

#include "Core/typedefs.h"

namespace Caravel {
namespace Integrals {

// ----------------------------------------------------------------------------
// -------------------------- MasterIntegral ----------------------------------
// ----------------------------------------------------------------------------

/**
 * Class to handle Master Integrals
 *
 * An \f$L\f$ - Loop amplitude can be decompised into Master Integrals
 * \f$ \mathcal{I}_i \f$ and coefficients that encode the kinematics:
 *
 * \f[
 * \mathcal{A}^{(L)} = c_i \cdot \mathcal{I}^{(L)}_i.
 * \f]
 *
 * In general the Master integrals can be evaluated by brute force integration
 * but in order to perform a numerical reconstruction of the amplitude it is
 * beneficial to express each Master Integral as a vector in a space some
 * basis functions. The coefficients of these basis elements are then
 * Series expansions in the dimensional regulator \f$ \epsilon \f$:
 *
 * \f[
 * \mathcal{I}^{(L)}_i = b_{ij}(\epsilon)\, \mathcal{B}_j
 * \f]
 *
 * where \f$ \mathcal{B}_j \f$ are the basis functions.
 * The basis function type is by FType. The type T denotes the type of the
 * coefficients in the series expasion in the dimensional regulator. <br>
 *
 * In the application case of Master Integrals in Loop calculations in QFT
 * one would express the Master Integral in a basis of special functions
 * among which Logarithms, Polylogarithms, multiple Polylogarithms, generalized
 * Polylogarithms, Goncharov Polylogarithms or Elliptic Functions appear. <br>
 *
 * TODO:
 * For the moment this is only a wrapper to the FunctionBasisExpansion. In the
 * future it should also add functionality to handle the efficient storage of
 * the basis functions and their numerical evaluation.
 */
template <typename CType, typename FType>
class MasterIntegral {
public:
    /// Default constructor
    MasterIntegral() = default;

    /**
     * Construct an integral from its basis elements.
     *
     * The basis elements are handed over as a vector of pairs, where each vector
     * entry consists of a basis element, the first entry in the pair being the
     * basis function and the second its corresponding coefficient.
     *
     * @param basis_elems The basis elements.
     */
    MasterIntegral(const std::vector<std::pair<FType, CType>>& basis_elems);

    /// Default virtual destructor
    virtual ~MasterIntegral() = default;

    /// Default copy constructor
    MasterIntegral(const MasterIntegral<CType, FType>& other) = default;

    /// Default copy assignment operator
    MasterIntegral<CType, FType>& operator=(MasterIntegral<CType, FType>& other) = default;

    /**
     * Return the number of functions in the basis.
     * @return The number of functions in the basis.
     */
    unsigned long n_basis_funcs() const;

    /**
     * Check whether a given basis function is contained in the basis.
     * @param  basis_func The basis function the check is performed for.
     * @return            true if the requested function is in the basis, else false.
     */
    bool contains_basis_function(const FType& basis_func) const;

    /**
     * Add a new basis function with its coefficient to the basis.
     *
     * NOTE: In the future, this function might become private or be replaced
     * by a dedicated constructor.
     *
     * @param basis_func The basis function to be added.
     * @param coeff      The coefficient of the basis function.
     */
    void add_basis_element(const FType& basis_func, const CType& coeff);

    /**
     * Compute the numerical values of all the coefficients of the basis functions.
     *
     * The values are stored for later retrieval.
     *
     * @param args Contains the numerical values of the kinematic invariants used
     *             to evaluate the coefficients.
     * @return     A numerical function basis expansion that can be
     *             further processed.
     */
    NumericFunctionBasisExpansion<typename CType::output_type, FType>
    eval_basis_coeffs(const std::vector<typename CType::input_type>& args);

    /**
     * Return the underlying abstract basis in function space.
     *
     * @return The abstract FunctionBasisExpansion of the master integral.
     */
    FunctionBasisExpansion<CType, FType> get_basis() const;

    /**
     * Evaluate the numeric value of the complete integral.
     *
     * This method is only defined if the input type of the basis function is not of integral
     * type since it makes not sense to evaluate certain basis functions with finite fields.
     * Furthermore since both the coefficients and the basis functions are evaluated with
     * the same input, both need to have identical input types.
     *
     * @param args The parameters (e.g. external kinematic invariants the itnegral depends on.)
     * @return     The numerical result of the integral.
     */
    template <typename TT = typename FType::input_type>
    typename std::enable_if<!is_exact<TT>::value &&
                                std::is_same<typename FType::input_type, typename CType::input_type>::value,
                            typename CType::output_type>::type
    compute(const std::vector<typename FType::input_type>& args);

protected:
    FunctionBasisExpansion<CType, FType> abstract_basis; /**< Handle the basis coefficients. */
};

} // namespace Integrals
} // namespace Caravel

// ----------------------------------------------------------------------------
// ------------------------ Implementations -----------------------------------
// ----------------------------------------------------------------------------

#include "MasterIntegral.hpp"

#endif // _MASTERINTEGRAL_H_
