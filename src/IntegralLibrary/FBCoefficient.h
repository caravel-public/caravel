#ifndef _FBCOEFFICIENT_H_
#define _FBCOEFFICIENT_H_

#include <vector>
#include <functional>

#include "Core/Series.h"
#include "IntegralLibrary/IntegralTraits.h"

#include "Core/typedefs.h"

namespace Caravel {
namespace Integrals {

// ----------------------------------------------------------------------------
// -------------------------- FBCoefficient -----------------------------------
// ----------------------------------------------------------------------------

/**
 * Class to the describe a coefficient of Master Integral basis function.
 *
 * Such a coefficient is on the one hand a Series expansion in the dimensional
 * regulator \f$ \epsilon \f$ which is defined as usual by a dimension shift
 * \f$ d = 4 - 2 \cdot \epsilon \f$.
 * <br>
 * On the other hand, the coefficient is also a rational function of kinematical
 * invariants constructed using the external momenta. Therefore it stores a
 * function that takes as argument a vector of invariants, thus allowing to
 * compute the value of a coefficient numerically.
 * <br>
 * The template parameter Out stands for the type of the output that is produced
 * when the Coefficient is evaluated numerically.
 * <br>
 * The template parameter In is the type of the variables that are inserted into
 * a coefficient to evaluate it numerically.
 */
template<typename Out, typename In>
class FBCoefficient : public InOutTrait<In, Out> {
public:
    /**
     * Construct a coefficient with a given function of the external invariants.
     * @param  f  The function that evaluates the coefficient numerically for
     *            a given set of kinematical invariants.
     */
    FBCoefficient(const std::function<Out(const std::vector<In>&)>& f);

    /**
     * The default copy constructor.
     * @param  c  The Coefficient whose content is to be copied into *this.
     */
    FBCoefficient(const FBCoefficient<Out, In>& c);

    /**
     * The default copy assignment operator.
     * @param  c    The Coefficient to be assigned to *this.
     * @return      A new coefficient with the content of c.
     */
    FBCoefficient<Out, In>& operator=(const FBCoefficient<Out, In>& c);

    /**
     * Compute the value of the coefficient for a given set of kinmatical
     * invariants.
     *
     * Note that the computed value is not only returned but also stored for
     * later retrieval.
     *
     * @param  args  Contains the numerical values of the kinmatical invariants.
     * @return       The numerical value of the coefficient evaluated for the
     *               given external momenta as a Series<T> in \f$ \epsilon \f$.
     */
    Out compute(const std::vector<In>& args);

    /**
     * Returnethe stored numerical value of the coefficient as a Series<T>
     * \f$ \epsilon \f$.
     * @return The stored numerical value of the coefficient as a Series<T> in
     *         \f$ \epsilon \f$.
     */
    Out get_value() const;
private:
    /**
     * The function that computes the numerical value of the coefficient as a
     * function of a set of kinematical invariants provided in a vector.
     */
    std::function<Out(const std::vector<In>&)> func;
    Out num_result;     /**< Stores the numerical value of the coefficient.*/
    bool is_evaluated{false}; /**< Indicates whether the coefficient has been evaluated. */
};

} // namespace Integrals
} // namespace Caravel

// IMPLEMENTATIONS
#include "FBCoefficient.hpp"

#endif // _FBCOEFFICIENT_H_
