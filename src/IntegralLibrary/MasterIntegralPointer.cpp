#include "MasterIntegralPointer.h"

#include <sstream>

namespace Caravel {
namespace Integrals {

bool compare(const InvariantSigns& is1,const InvariantSigns& is2){
    if(is1==InvariantSigns::anything||is2==InvariantSigns::anything)
        return true;
    else if(is1==InvariantSigns::undefined||is2==InvariantSigns::undefined){
        std::cerr<<"ERROR: compare(InvariantSigns,InvariantSigns) can't deal with undefined values!"<<std::endl;
        std::exit(1);
    }
    else
        return is1==is2;
}

bool compare(const std::vector<InvariantSigns>& v1,const std::vector<InvariantSigns>& v2){
    // sanity check
    if(v1.size()!=v2.size()){
        std::cerr<<"ERROR: compare(vector<InvariantSigns>,vector<InvariantSigns>) received vectors of different size!"<<std::endl;
        std::exit(1);
    }
    for(size_t ii=0;ii<v1.size();ii++)    if(!compare(v1[ii],v2[ii]))    return false;
    return true;
}

}
} // namespace Caravel
