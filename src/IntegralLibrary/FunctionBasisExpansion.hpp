#include <sstream>
#include <map>
#include <string>


namespace Caravel {
namespace Integrals {

// ----------------------------------------------------------------------------
// ------------------- NumericFunctionBasisExpansion --------------------------
// ----------------------------------------------------------------------------
template <typename CType, typename FType>
NumericFunctionBasisExpansion<CType, FType>::NumericFunctionBasisExpansion(const std::vector<FType>& fs, const std::vector<CType>& cs)
    : NumericFunctionBasisExpansion([&](){

        // Assert that the inputs are valid.
        if (fs.size() != cs.size()){
            throw std::runtime_error("List of functions and list of coefficients not the same size in construction of NumericFunctionBasisExpansion. fs.size() = " +
            std::to_string(fs.size()) + ", cs.size() = " + std::to_string(cs.size()) );
        }

        // Pack them into an unordered map.
        std::unordered_map<FType, CType> b;
        for (size_t i = 0; i < fs.size(); i++){
            b[fs.at(i)] = cs.at(i);
        }

        return b;
    }()) {}

template <typename CType, typename FType>
NumericFunctionBasisExpansion<CType, FType>::NumericFunctionBasisExpansion(const std::unordered_map<FType, CType>& b)
    : basis{b} {}

template <typename CType, typename FType>
unsigned long NumericFunctionBasisExpansion<CType, FType>::n_basis_funcs() const {
    return basis.size();
}

template <typename CType, typename FType>
bool NumericFunctionBasisExpansion<CType, FType>::contains(const FType& basis_func) const {
    return (basis.find(basis_func) != basis.end()) ? true : false;
}

template <typename CType, typename FType>
CType NumericFunctionBasisExpansion<CType, FType>::get_coeff(const FType& basis_func) const {
    auto pos = basis.find(basis_func);
    if (pos == basis.end()) {
        throw std::runtime_error("The corresponding function is not in the basis!");
    } else {
        return pos->second;
    }
}

template <typename CType, typename FType>
std::vector<FType> NumericFunctionBasisExpansion<CType, FType>::get_all_basis_functions() const {
    std::vector<FType> toret;
    for (auto elem : basis) { toret.push_back(elem.first); }
    return toret;
}

template <typename CType, typename FType>
std::vector<CType> NumericFunctionBasisExpansion<CType, FType>::get_all_coeffs() const {
    std::vector<CType> toret;
    for (auto elem : basis) { toret.push_back(elem.second); }
    return toret;
}

template <typename CType, typename FType>
auto NumericFunctionBasisExpansion<CType, FType>::begin() {
    return basis.begin();
}

template <typename CType, typename FType>
auto NumericFunctionBasisExpansion<CType, FType>::begin() const {
    return basis.begin();
}

template <typename CType, typename FType>
auto NumericFunctionBasisExpansion<CType, FType>::end() {
    return basis.end();
}

template <typename CType, typename FType>
auto NumericFunctionBasisExpansion<CType, FType>::end() const {
    return basis.end();
}

template <typename CType, typename FType>
template <typename TT>
typename std::enable_if<is_series<TT>::value, NumericFunctionBasisExpansion<typename TT::coeff_type, FType>>::type
NumericFunctionBasisExpansion<CType, FType>::get_eps_pow_coeff(int power) const {
    // Declare the type of a coefficient of the Series
    typedef typename TT::coeff_type CT;
    std::unordered_map<FType, CT> map;
    for (auto elem : basis) {
        if (elem.second[power] != static_cast<CT>(0)) { map.insert({elem.first, elem.second[power]}); }
    }
    return NumericFunctionBasisExpansion<typename TT::coeff_type, FType>(map);
}

template <typename CType, typename FType>
template <typename TT>
typename std::enable_if<is_series<TT>::value, int>::type NumericFunctionBasisExpansion<CType, FType>::leading() const {
    int min = basis.begin()->second.leading();
    for (const auto& elem : basis) {
        if (elem.second.leading() < min) { min = elem.second.leading(); }
    }
    return min;
}

template <typename CType, typename FType>
template <typename TT>
typename std::enable_if<is_series<TT>::value, int>::type NumericFunctionBasisExpansion<CType, FType>::last() const {
    int max = basis.begin()->second.last();
    for (const auto& elem : basis) {
        if (elem.second.last() > max) { max = elem.second.last(); }
    }
    return max;
}

// Instance operators

template <typename CType, typename FType>
NumericFunctionBasisExpansion<CType, FType>& NumericFunctionBasisExpansion<CType, FType>::
operator+=(const NumericFunctionBasisExpansion<CType, FType>& other) {
    // Step through all the entries in the basis of other
    for (const auto& elem : other.basis) {
        // If the given basis function in other also appears in this, increase the coefficient.
        auto pos = basis.find(elem.first);
        if (pos != basis.end()) { pos->second = pos->second + elem.second; }
    }
    // Add the basis functions including coefficients that are not contained in *this yet.
    basis.insert(other.basis.begin(), other.basis.end());
    return *this;
}

template <typename CType, typename FType>
NumericFunctionBasisExpansion<CType, FType>& NumericFunctionBasisExpansion<CType, FType>::
operator-=(const NumericFunctionBasisExpansion<CType, FType>& other) {
    // Step through all the entries in the basis of other
    for (const auto& elem : other.basis) {
        // If the given basis function in other also appears in this, increase the coefficient.
        auto pos = basis.find(elem.first);
        if (pos != basis.end()) { pos->second = pos->second - elem.second; }
    }
    // Add the basis functions including coefficients that are not contained in *this yet.
    for (const auto& elem : other.basis) {
        // The ones that are already in will be ignored
        basis.insert({elem.first, -elem.second});
    }

    return *this;
}

template <typename CType, typename FType>
NumericFunctionBasisExpansion<CType, FType>& NumericFunctionBasisExpansion<CType, FType>::
operator*=(const FType& func) {
    std::unordered_map<FType, CType> result;

    for (auto& el : basis) {
        auto temp = el.first * func;
        result[temp] = el.second;
    }

    *this = NumericFunctionBasisExpansion<CType, FType>(result);

    return (*this);
}

template <typename CType, typename FType>
NumericFunctionBasisExpansion<CType, FType>& NumericFunctionBasisExpansion<CType, FType>::
operator*=(const NumericFunctionBasisExpansion<CType, FType>& other) {
    auto copy = *this;

    NumericFunctionBasisExpansion<CType, FType> result;
    for (auto& el : other) { result += el.first * (el.second * copy); }

    *this = result;

    return (*this);
}

template <typename CType, typename FType>
template <typename T>
NumericFunctionBasisExpansion<CType, FType>& NumericFunctionBasisExpansion<CType, FType>::operator*=(const T& scal) {
    // Multiply every coefficient in the basis by scal:
    for (auto& elem : basis) { elem.second = elem.second * scal; }
    return *this;
}

template <typename CType, typename FType>
template <typename T>
NumericFunctionBasisExpansion<CType, FType>& NumericFunctionBasisExpansion<CType, FType>::operator/=(const T& scal) {
    // Divide every coefficient in the basis by scal:
    for (auto& elem : basis) { elem.second = elem.second / scal; }
    return *this;
}

template <typename CType, typename FType>
template <typename CT, typename FT>
std::enable_if_t<
    is_series<CT>::value && is_std_basis_function<FT>::value && !is_exact<typename CT::coeff_type>::value, CT>
NumericFunctionBasisExpansion<CType, FType>::operator()(const std::vector<typename FT::input_type>& args,
                                                        const std::vector<std::string>& names) {
    CT ret(leading(), last());
    for (const auto& elem : basis) { ret += elem.first(args, names) * elem.second; }
    return ret;
}

// Operators

template <typename CType, typename FType>
std::ostream& operator<<(std::ostream& stream, const NumericFunctionBasisExpansion<CType, FType>& expression) {
    bool first = true;
    std::map<FType, CType> ordered(expression.begin(), expression.end());
    for (auto& term : ordered) {
        if (term.second == 0) { continue; }
        if (!first) { stream << " + " << std::endl; }
        first = false;
        stream << term.first << " * " << term.second;
    }

    return stream;
}

template <typename CType, typename FType>
NumericFunctionBasisExpansion<CType, FType> operator+(const NumericFunctionBasisExpansion<CType, FType>& b1,
                                                      const NumericFunctionBasisExpansion<CType, FType>& b2) {
    auto toreturn(b1);
    toreturn += b2;
    return toreturn;
}

template <typename CType, typename FType>
NumericFunctionBasisExpansion<CType, FType> operator-(const NumericFunctionBasisExpansion<CType, FType>& b1,
                                                      const NumericFunctionBasisExpansion<CType, FType>& b2) {
    auto toreturn(b1);
    toreturn -= b2;
    return toreturn;
}

template <typename CType, typename FType>
NumericFunctionBasisExpansion<CType, FType> operator*(NumericFunctionBasisExpansion<CType, FType> expr1,
                                                      const NumericFunctionBasisExpansion<CType, FType>& expr2) {
    return (expr1 *= expr2);
}

template <typename CType, typename FType>
NumericFunctionBasisExpansion<CType, FType> operator*(const FType& func,
                                                      NumericFunctionBasisExpansion<CType, FType> expr) {
    return (expr *= func);
}

template <typename CType, typename FType, typename T>
NumericFunctionBasisExpansion<CType, FType> operator*(const T& scal,
                                                      const NumericFunctionBasisExpansion<CType, FType>& basis) {
    auto toreturn(basis);
    toreturn *= scal;
    return toreturn;
}

template <typename CType, typename FType, typename T>
NumericFunctionBasisExpansion<CType, FType> operator*(const NumericFunctionBasisExpansion<CType, FType>& basis,
                                                      const T& scal) {
    return scal * basis;
}

template <typename CType, typename FType, typename T>
NumericFunctionBasisExpansion<CType, FType> operator/(const NumericFunctionBasisExpansion<CType, FType>& basis,
                                                      const T& scal) {
    auto toreturn(basis);
    toreturn /= scal;
    return toreturn;
}

template <typename CType, typename FType>
void NumericFunctionBasisExpansion<CType, FType>::select(
    const std::function<bool(const std::pair<FType, CType>&)>& predicate) {

    for (auto it = begin(); it != end();) {
        if (!predicate(*it)) {
            it = basis.erase(it);
        } else {
            it++;
        }
    }
}

template <typename CType, typename FType>
template <typename NewCType>
NumericFunctionBasisExpansion<NewCType, FType>
NumericFunctionBasisExpansion<CType, FType>::coeff_map(const std::function<NewCType(CType)>& f) {

    std::unordered_map<FType, NewCType> new_terms;

    for (auto it = begin(); it != end(); it++) { new_terms[it->first] = f(it->second); }

    return {new_terms};
}

// ----------------------------------------------------------------------------
// ------------------- FunctionBasisExpansion ---------------------------------
// ----------------------------------------------------------------------------
template <typename CType, typename FType>
unsigned long FunctionBasisExpansion<CType, FType>::n_basis_funcs() const {
    return basis.size();
}

template <typename CType, typename FType>
bool FunctionBasisExpansion<CType, FType>::contains(const FType& basis_func) const {
    auto pos = basis.find(basis_func);
    if (pos != basis.end()) {
        return true;
    } else {
        return false;
    }
}

template <typename CType, typename FType>
void FunctionBasisExpansion<CType, FType>::add_basis_element(const FType& basis_func, const CType& coeff) {
    // Abstract coefficient cannot be added. Problem if there is already
    // a coefficient inside. Throw
    auto pos = basis.find(basis_func);
    if (pos != basis.end()) {
        std::ostringstream oss;
        oss << "Trying to add basis function '" << basis_func.as_string() << "' failed as it is already contained!";
        throw std::runtime_error(oss.str());
    }
    // Add the function and its coefficient as a new entry.
    basis.insert({basis_func, coeff});
}

template <typename CType, typename FType>
NumericFunctionBasisExpansion<typename CType::output_type, FType>
FunctionBasisExpansion<CType, FType>::eval_coeffs(const std::vector<typename CType::input_type>& args) const {
    std::unordered_map<FType, typename CType::output_type> toreturn;
    for (auto elem : basis) { toreturn.insert({elem.first, elem.second.compute(args)}); }
    return NumericFunctionBasisExpansion<typename CType::output_type, FType>(toreturn);
}

template <typename CType, typename FType>
template <typename TT>
typename std::enable_if<!is_exact<TT>::value &&
                            std::is_same<typename FType::input_type, typename CType::input_type>::value,
                        typename CType::output_type>::type // Series<typename FType::output_type>
FunctionBasisExpansion<CType, FType>::eval_complete(const std::vector<typename FType::input_type>& args) const {
    auto res = eval_coeffs(args);
    // Set the toreturn value to the first coefficient, times the first basis function.
    // We cannot first set a zero value because we need to know the epsilon powers.
    auto function = res.begin()->first;
    auto toreturn = res.begin()->second * function(args); // * basis_functions.at(res.begin()->first)(args);

    // Now from the second basis function on, add the others to the result.
    for (auto it = ++res.begin(); it != res.end(); ++it) {
        // Need to first copy the basis function into function because keys cannot be modified!
        function = it->first;
        toreturn = toreturn + it->second * function(args);
    }

    return toreturn;
}

template <typename CType, typename FType>
std::string math_string(const Integrals::NumericFunctionBasisExpansion<CType, FType>& expression) {
    using ::Caravel::math_string;
    std::stringstream stream;
    bool first = true;
    std::map<FType, CType> ordered(expression.begin(), expression.end());
    for (auto& term : ordered) {
        if (term.second == 0) { continue; }
        if (!first) { stream << " + " << std::endl; }
        first = false;
        stream << math_string(term.first) << " * " << "(" << math_string(term.second) << ")";
    }
    return stream.str();
}


// ----------------------------------------------------------------------------
// ------------ Declare external instantiations -------------------------------
// ----------------------------------------------------------------------------

// Note: In the future DummyBasisFunction will be replaced by whatever type
// we use.

#define EXTERN_INSTANTIATE_FBE(OUT, IN)                                                                                \
    extern template class FunctionBasisExpansion<FBCoefficient<Series<OUT>, IN>, StdBasisFunction<OUT, IN>>;

EXTERN_INSTANTIATE_FBE(C, R)
#ifdef HIGH_PRECISION
EXTERN_INSTANTIATE_FBE(CHP, RHP)
#endif
#ifdef VERY_HIGH_PRECISION
EXTERN_INSTANTIATE_FBE(CVHP, RVHP)
#endif

#ifdef USE_FINITE_FIELDS
#define EXTERN_INSTANTIATE_FBE_FF(CIN, COUT, OUT, IN)                                                                  \
    extern template class FunctionBasisExpansion<FBCoefficient<Series<COUT>, CIN>, StdBasisFunction<OUT, IN>>;

EXTERN_INSTANTIATE_FBE_FF(F32, F32, C, R)
#ifdef HIGH_PRECISION
EXTERN_INSTANTIATE_FBE_FF(F32, F32, CHP, RHP)
#endif
#ifdef VERY_HIGH_PRECISION
EXTERN_INSTANTIATE_FBE_FF(F32, F32, CVHP, RVHP)
#endif

#ifdef INSTANTIATE_RATIONAL
EXTERN_INSTANTIATE_FBE_FF(BigRat, F32, C, R)
#ifdef HIGH_PRECISION
EXTERN_INSTANTIATE_FBE_FF(BigRat, F32, CHP, RHP)
#endif
#ifdef VERY_HIGH_PRECISION
EXTERN_INSTANTIATE_FBE_FF(BigRat, F32, CVHP, RVHP)
#endif
#endif

#endif

#define EXTERN_INSTANTIATE_NFBE(OUT, IN)                                                                               \
    extern template class NumericFunctionBasisExpansion<Series<OUT>, StdBasisFunction<OUT, IN>>;                       \
                                                                                                                       \
    extern template NumericFunctionBasisExpansion<Series<OUT>, StdBasisFunction<OUT, IN>> operator+(                   \
        const NumericFunctionBasisExpansion<Series<OUT>, StdBasisFunction<OUT, IN>>& b1,                               \
        const NumericFunctionBasisExpansion<Series<OUT>, StdBasisFunction<OUT, IN>>& b2);                              \
                                                                                                                       \
    extern template NumericFunctionBasisExpansion<Series<OUT>, StdBasisFunction<OUT, IN>> operator-(                   \
        const NumericFunctionBasisExpansion<Series<OUT>, StdBasisFunction<OUT, IN>>& b1,                               \
        const NumericFunctionBasisExpansion<Series<OUT>, StdBasisFunction<OUT, IN>>& b2);

EXTERN_INSTANTIATE_NFBE(C, R)

#ifdef HIGH_PRECISION
EXTERN_INSTANTIATE_NFBE(CHP, RHP)
#endif

#ifdef VERY_HIGH_PRECISION
EXTERN_INSTANTIATE_NFBE(CVHP, RVHP)
#endif

#ifdef USE_FINITE_FIELDS
#define EXTERN_INSTANTIATE_NFBE_FF(COUT, OUT, IN)                                                                      \
    extern template class NumericFunctionBasisExpansion<Series<COUT>, StdBasisFunction<OUT, IN>>;                      \
                                                                                                                       \
    extern template NumericFunctionBasisExpansion<Series<COUT>, StdBasisFunction<OUT, IN>> operator+(                  \
        const NumericFunctionBasisExpansion<Series<COUT>, StdBasisFunction<OUT, IN>>& b1,                              \
        const NumericFunctionBasisExpansion<Series<COUT>, StdBasisFunction<OUT, IN>>& b2);                             \
                                                                                                                       \
    extern template NumericFunctionBasisExpansion<Series<COUT>, StdBasisFunction<OUT, IN>> operator-(                  \
        const NumericFunctionBasisExpansion<Series<COUT>, StdBasisFunction<OUT, IN>>& b1,                              \
        const NumericFunctionBasisExpansion<Series<COUT>, StdBasisFunction<OUT, IN>>& b2);

EXTERN_INSTANTIATE_NFBE_FF(F32, C, R)
#ifdef HIGH_PRECISION
EXTERN_INSTANTIATE_NFBE_FF(F32, CHP, RHP)
#endif
#ifdef VERY_HIGH_PRECISION
EXTERN_INSTANTIATE_NFBE_FF(F32, CVHP, RVHP)
#endif

#ifdef INSTANTIATE_RATIONAL
EXTERN_INSTANTIATE_NFBE_FF(BigRat, C, R)
#ifdef HIGH_PRECISION
EXTERN_INSTANTIATE_NFBE_FF(BigRat, CHP, RHP)
#endif
#ifdef VERY_HIGH_PRECISION
EXTERN_INSTANTIATE_NFBE_FF(BigRat, CVHP, RVHP)
#endif
#endif
#endif

#define EXTERN_INSTANTIATE_OPERATOR_SCALAR(OUT, IN, TYPE)                                                              \
    extern template NumericFunctionBasisExpansion<Series<OUT>, StdBasisFunction<OUT, IN>> operator*(                   \
        const TYPE& scal, const NumericFunctionBasisExpansion<Series<OUT>, StdBasisFunction<OUT, IN>>& basis);         \
                                                                                                                       \
    extern template NumericFunctionBasisExpansion<Series<OUT>, StdBasisFunction<OUT, IN>> operator*<>(                 \
        const NumericFunctionBasisExpansion<Series<OUT>, StdBasisFunction<OUT, IN>>& basis, const TYPE& scal);         \
                                                                                                                       \
    extern template NumericFunctionBasisExpansion<Series<OUT>, StdBasisFunction<OUT, IN>> operator/                    \
        <>(const NumericFunctionBasisExpansion<Series<OUT>, StdBasisFunction<OUT, IN>>& basis, const TYPE& scal);

// EXTERN_INSTANTIATE_OPERATOR_SCALAR(C, R, R)
EXTERN_INSTANTIATE_OPERATOR_SCALAR(C, R, C)

#ifdef HIGH_PRECISION
// EXTERN_INSTANTIATE_OPERATOR_SCALAR(CHP, RHP, RHP)
EXTERN_INSTANTIATE_OPERATOR_SCALAR(CHP, RHP, CHP)
#endif

#ifdef VERY_HIGH_PRECISION
// EXTERN_INSTANTIATE_OPERATOR_SCALAR(CVHP, RVHP, RVHP)
EXTERN_INSTANTIATE_OPERATOR_SCALAR(CVHP, RVHP, CVHP)
#endif

#ifdef USE_FINITE_FIELDS
#define EXTERN_INSTANTIATE_OPERATOR_SCALAR_FF(COUT, OUT, IN, TYPE)                                                     \
    extern template NumericFunctionBasisExpansion<Series<COUT>, StdBasisFunction<OUT, IN>> operator*(                  \
        const TYPE& scal, const NumericFunctionBasisExpansion<Series<COUT>, StdBasisFunction<OUT, IN>>& basis);        \
                                                                                                                       \
    extern template NumericFunctionBasisExpansion<Series<COUT>, StdBasisFunction<OUT, IN>> operator*<>(                \
        const NumericFunctionBasisExpansion<Series<COUT>, StdBasisFunction<OUT, IN>>& basis, const TYPE& scal);        \
                                                                                                                       \
    extern template NumericFunctionBasisExpansion<Series<COUT>, StdBasisFunction<OUT, IN>> operator/                   \
        <>(const NumericFunctionBasisExpansion<Series<COUT>, StdBasisFunction<OUT, IN>>& basis, const TYPE& scal);

EXTERN_INSTANTIATE_OPERATOR_SCALAR_FF(F32, C, R, F32)
#ifdef HIGH_PRECISION
EXTERN_INSTANTIATE_OPERATOR_SCALAR_FF(F32, CHP, RHP, F32)
#endif
#ifdef VERY_HIGH_PRECISION
EXTERN_INSTANTIATE_OPERATOR_SCALAR_FF(F32, CVHP, RVHP, F32)
#endif

#ifdef INSTANTIATE_RATIONAL
EXTERN_INSTANTIATE_OPERATOR_SCALAR_FF(BigRat, C, R, BigRat)
#ifdef HIGH_PRECISION
EXTERN_INSTANTIATE_OPERATOR_SCALAR_FF(BigRat, CHP, RHP, BigRat)
#endif
#ifdef VERY_HIGH_PRECISION
EXTERN_INSTANTIATE_OPERATOR_SCALAR_FF(BigRat, CVHP, RVHP, BigRat)
#endif
#endif
#endif

} // namespace Integrals
} // namespace Caravel
