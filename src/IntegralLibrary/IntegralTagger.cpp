#include "IntegralTagger.h"

namespace Caravel {
namespace Integrals {

using Caravel::operator<<;

namespace detail {

std::vector<size_t> mom_conservation(size_t n, const std::vector<size_t>& indices) {
    std::vector<size_t> o;
    for (size_t ii = 1; ii <= n; ii++)
        if (std::find(indices.begin(), indices.end(), ii) == indices.end()) o.push_back(ii);
    return o;
}

std::pair<InvariantSigns, std::string> sij_identifier(size_t n, const std::vector<size_t>& initial, const std::vector<size_t>& exit,
                                           const std::vector<std::vector<size_t>>& momenta_from_indices, const std::vector<size_t>& indices,
                                           const lGraph::xGraph& stored, const lGraph::xGraph& out) {
    // sanity check
    std::set<size_t> indexset;
    for(const auto& i:indices)    indexset.insert(i);
    if(indexset.size()!=indices.size()){
        std::cerr<<"ERROR: InvariantType::Mandelstam should not have momentum_indices repeated. Received: "<<indices<<std::endl;
        std::exit(4444);
    }

    InvariantSigns itoret(InvariantSigns::anything);
    std::string storet = "s";

    std::vector<size_t> oindices;
    for (auto i : indices) oindices.insert(oindices.end(), momenta_from_indices[i - 1].begin(), momenta_from_indices[i - 1].end());
    std::sort(oindices.begin(), oindices.end());

    // come sorted
    std::vector<size_t> alt_oindices = mom_conservation(n, oindices);

    if (alt_oindices.size() < oindices.size())
        oindices = alt_oindices;
    else if (alt_oindices.size() == oindices.size())
        // lexicographic
        if (std::lexicographical_compare(alt_oindices.begin(), alt_oindices.end(), oindices.begin(), oindices.end())) oindices = alt_oindices;

    for (const auto& i : oindices) storet += std::to_string(i);

    size_t intialincluded(0);
    for (const auto& i : oindices)
        if (std::find(initial.begin(), initial.end(), i) == initial.end()) intialincluded++;

    if(initial.size()==0)
      itoret=InvariantSigns::negative;
    else if(intialincluded==0)
        itoret=InvariantSigns::positive;
    else if(intialincluded==initial.size())
        itoret=InvariantSigns::positive;
    else if(intialincluded==oindices.size())
        itoret=InvariantSigns::positive;
    else
        itoret=InvariantSigns::negative;

    return {itoret, storet};
}

std::pair<InvariantSigns, std::string> mass_squared_identifier(size_t n, const std::vector<size_t>& initial, const std::vector<size_t>& exit,
                                           const std::vector<std::vector<size_t>>& momenta_from_indices, const std::vector<size_t>& indices,
                                           const lGraph::xGraph& stored, const lGraph::xGraph& out) {
    // sanity check
    if(indices.size()!=1){
        std::cerr<<"ERROR: InvariantType::Mass2 should have a single entry in momentum_indices. Received: "<<indices<<std::endl;
        std::exit(4445);
    }

    // check if it corresponds to a Mandelstam variable
    if(momenta_from_indices[indices[0]-1].size()>1){
        auto lindices = momenta_from_indices[indices[0]-1];
        // 1-1 correspondence
        std::vector<std::vector<size_t>> lmomenta_from_indices(n);
        for(size_t ii=1;ii<=n;ii++)
            lmomenta_from_indices[ii-1].push_back(ii);
        return sij_identifier(n, initial, exit, lmomenta_from_indices, lindices, stored, out);
    }

    InvariantSigns itoret(InvariantSigns::anything);
    std::string storet = "mSqr";

    std::vector<size_t> oindices;
    for (auto i : indices) oindices.insert(oindices.end(), momenta_from_indices[i - 1].begin(), momenta_from_indices[i - 1].end());
    std::sort(oindices.begin(), oindices.end());

    // come sorted
    std::vector<size_t> alt_oindices = mom_conservation(n, oindices);

    if (alt_oindices.size() < oindices.size())
        oindices = alt_oindices;
    else if (alt_oindices.size() == oindices.size())
        // lexicographic
        if (std::lexicographical_compare(alt_oindices.begin(), alt_oindices.end(), oindices.begin(), oindices.end())) oindices = alt_oindices;

    for (const auto& i : oindices) storet += std::to_string(i);

    size_t intialincluded(0);
    for (const auto& i : oindices)
        if (std::find(initial.begin(), initial.end(), i) == initial.end()) intialincluded++;

    if(initial.size()==0)
      itoret=InvariantSigns::negative;
    else if(intialincluded==0)
        itoret=InvariantSigns::positive;
    else if(intialincluded==initial.size())
        itoret=InvariantSigns::positive;
    else if(intialincluded==oindices.size())
        itoret=InvariantSigns::positive;
    else
        itoret=InvariantSigns::negative;

    return {itoret, storet};
}

void tr5_index_name_pick(size_t n, std::vector<size_t>& indices){
    if(n != 5){
        std::cout<<"WARNING: picking tr5 standard naming for "<<n<<" momenta not yet implemented!"<<std::endl;
        std::exit(1);
    }
    if(indices.size() != 4){
        std::cout<<"WARNING: picking tr5 standard naming should have got 4 indices, but received: "<<indices<<std::endl;
        std::exit(1);
    }

    // the missing mom
    auto vother = mom_conservation(n, indices);
    int other = int(vother[0]);
    int sign(1);
    if(other!=5){
        sign*=-1;
        std::replace(indices.begin(),indices.end(), 5, other);
    }
    // bring 1 to front
    auto oneIt = std::find(indices.begin(), indices.end(), 1);
    size_t one_distance = std::distance(indices.begin(), oneIt);
    for(size_t ii=0; ii<one_distance; ii++)    sign*=-1;
    indices.erase(oneIt);
    // and bring 2 to second position (after erasing 1, its first)
    auto twoIt = std::find(indices.begin(), indices.end(), 2);
    size_t two_distance = std::distance(indices.begin(), twoIt);
    for(size_t ii=0; ii<two_distance; ii++)    sign*=-1;
    indices.erase(twoIt);
    if(indices[0] == 4)    sign*=-1;
    if(sign==1)
        indices = { 1, 2, 3, 4 };
    else
        indices = { 1, 2, 4, 3 };
}

std::pair<InvariantSigns, std::string> tr5_identifier(size_t n, const std::vector<size_t>& initial, const std::vector<size_t>& exit,
                                           const std::vector<std::vector<size_t>>& momenta_from_indices, const std::vector<size_t>& indices,
                                           const lGraph::xGraph& stored, const lGraph::xGraph& out) {
    InvariantSigns itoret(InvariantSigns::anything);
    std::string storet = "tr5";

    std::vector<size_t> oindices;
    for (auto i : indices) oindices.insert(oindices.end(), momenta_from_indices[i - 1].begin(), momenta_from_indices[i - 1].end());

    tr5_index_name_pick(n, oindices);

    for (const auto& i : oindices) storet += std::to_string(i);

    return {itoret, storet};
}

std::pair<InvariantSigns, std::string> twistor5_identifier(size_t n, const std::vector<size_t>& initial, const std::vector<size_t>& exit,
                                           const std::vector<std::vector<size_t>>& momenta_from_indices, const std::vector<size_t>& indices,
                                           const lGraph::xGraph& stored, const lGraph::xGraph& out) {
    InvariantSigns itoret(InvariantSigns::anything);
    std::string storet = "x";

    // sanity check
    if(indices.size()!=1){
        std::cerr<<"ERROR: InvariantType::Twistor5 can have only a single index! Received: "<<indices<<std::endl;
        std::exit(1);
    }
    if(indices[0]>4){
        std::cerr<<"ERROR: the index in InvariantType::Twistor5 has to be smaller than 4 and recieved: "<<indices[0]<<std::endl;
        std::exit(1);
    }

    storet+=std::to_string(indices[0]);

    return {itoret, storet};
}

std::pair<InvariantSigns, std::string> sign_tr5_identifier(size_t n, const std::vector<size_t>& initial, const std::vector<size_t>& exit,
                                           const std::vector<std::vector<size_t>>& momenta_from_indices, const std::vector<size_t>& indices,
                                           const lGraph::xGraph& stored, const lGraph::xGraph& out) {
    InvariantSigns itoret(InvariantSigns::anything);
    std::string storet = "signtr5";

    std::vector<size_t> oindices;
    for (auto i : indices) oindices.insert(oindices.end(), momenta_from_indices[i - 1].begin(), momenta_from_indices[i - 1].end());

    tr5_index_name_pick(n, oindices);

    for (const auto& i : oindices) storet += std::to_string(i);

    return {itoret, storet};
}

} // namespace detail

std::pair<InvariantSigns, std::string> invariant_sign_and_name(size_t n, const std::vector<size_t>& initial, const std::vector<size_t>& exit,
                                                    const std::vector<std::vector<size_t>>& momenta_from_indices, const InputVariable& variable,
                                                    const lGraph::xGraph& stored, const lGraph::xGraph& out) {
    InvariantSigns sign(InvariantSigns::anything);
    std::string name;

    if (variable.invariant_type == InvariantType::Mandelstam) { std::tie(sign, name) = detail::sij_identifier(n, initial, exit, momenta_from_indices, variable.momentum_indices, stored, out); }
    else if (variable.invariant_type == InvariantType::Mass2) { std::tie(sign, name) = detail::mass_squared_identifier(n, initial, exit, momenta_from_indices, variable.momentum_indices, stored, out); }
    else if (variable.invariant_type == InvariantType::Tr5) { std::tie(sign, name) = detail::tr5_identifier(n, initial, exit, momenta_from_indices, variable.momentum_indices, stored, out); }
    else if (variable.invariant_type == InvariantType::Twistor5) { std::tie(sign, name) = detail::twistor5_identifier(n, initial, exit, momenta_from_indices, variable.momentum_indices, stored, out); }
    else if (variable.invariant_type == InvariantType::SignTr5) { std::tie(sign, name) = detail::sign_tr5_identifier(n, initial, exit, momenta_from_indices, variable.momentum_indices, stored, out); }
    else{
        std::cerr<<"ERROR: IntegralTagger(.) can't process yet invariant type "<<variable.invariant_type<<std::endl;
        std::exit(4446);
    }

    if (sign == InvariantSigns::anything) {
        DEBUG(
            std::cerr<<"Any sign assigned for variable: "<<variable<<std::endl;
        );
    }
    else if (sign == InvariantSigns::undefined) {
        std::cerr<<"Couldn't find sign of variable: "<<variable<<std::endl;
        stored.show();
        out.show();
        std::exit(1);
    }
    return {sign, name};
}

std::pair<std::vector<InvariantSigns>, std::vector<std::string>> IntegralTagger(const std::vector<InputVariable>& variables, 
                                                                     const std::vector<size_t>& permutation, const lGraph::xGraph& stored,
                                                                     const lGraph::xGraph& out) {
    // sanity checks
    if (stored.get_topology() != out.get_topology()) {
        std::cerr << "ERROR: IntegralTagger(.) received unrelated graphs in arguments:" << std::endl;
        stored.show();
        out.show();
        std::exit(5284201);
    }
    // no check in case Euclidean
    if (PhysRegSelector::get_n() > 0) {
        if (PhysRegSelector::get_n() != out.get_n_legs()) {
            std::cerr << "ERROR: IntegralTagger(.) received 'out' xGraph with " << out.get_n_legs() << " momenta while physical region defined for "
                      << PhysRegSelector::get_n() << " points!" << std::endl;
            std::exit(5284202);
        }
    }

    std::vector<InvariantSigns> region(variables.size());
    std::vector<std::string> names(variables.size());

    size_t n = PhysRegSelector::get_n();
    const std::vector<size_t>& initial = PhysRegSelector::get_initial();
    std::vector<size_t> exit = {};
    // Euclidean
    if (n == 0) {
        n = out.get_n_legs();
        for (size_t ii = 1; ii <= n; ii++) exit.push_back(ii);
    }

    using namespace Caravel::lGraph::lGraphKin;

    // the index map
    auto momenta_from_indices_before = abstract_external_momentum_mapper(out,stored);
    decltype(momenta_from_indices_before) momenta_from_indices(momenta_from_indices_before.size());
    for(size_t ii=0;ii<momenta_from_indices_before.size();ii++)    momenta_from_indices[ii] = momenta_from_indices_before[permutation[ii]];

    for (size_t ii = 0; ii < variables.size(); ii++)
        std::tie(region[ii], names[ii]) = invariant_sign_and_name(n, initial, exit, momenta_from_indices, variables[ii], stored, out);

    return {region, names};
}

} // namespace Integrals
} // namespace Caravel
