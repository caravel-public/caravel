#include "FBCoefficient.h"

namespace Caravel {
namespace Integrals {

#define INSTANTIATE_COEFF(OUT, IN)  \
template class FBCoefficient<OUT, IN>;

INSTANTIATE_COEFF(Series<C>, R)
#ifdef HIGH_PRECISION
INSTANTIATE_COEFF(Series<CHP>, RHP)
#endif
#ifdef VERY_HIGH_PRECISION
INSTANTIATE_COEFF(Series<CVHP>, RVHP)
#endif

#ifdef USE_FINITE_FIELDS
INSTANTIATE_COEFF(Series<F32>, F32)
#endif

} // namespace Integrals
} // namespace Caravel
