/**
 * @file IntegralTagger.h
 *
 * @date 15.7.2019
 *
 * @brief Tools to help build abstract information for instances of integral classes constructed
 */
#ifndef INTEGRALTAGGER_H_
#define INTEGRALTAGGER_H_

#include <iostream>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <string>
#include <tuple>
#include <set>

#include "Core/Utilities.h"
#include "Core/PhysRegSelector.h"
#include "Graph/Graph.h"
#include "Graph/GraphKin.h"
#include "InputVariables.h"
#include "MasterIntegralPointer.h"
#include "Core/Debug.h"

namespace Caravel {
namespace Integrals {

/**
 * Tool to select the physical region and variable names associated to a
 * given set of variables and related xGraphs
 *
 * @param vector of identifiers for the variables
 * @param permutation of indices from customized integral choice (identity if non)
 * @param the xGraph related to the definition of the variables
 * @param associated xGraph for which we will produce the output
 * @return a pair containing:
 *       first:  a vector of signs taken by the input variables (picks the corresponding physical region)
 *       second: a vector of string that names the input variables
 */
std::pair<std::vector<InvariantSigns>, std::vector<std::string>> IntegralTagger(const std::vector<InputVariable>& variables,
                                                                     const std::vector<size_t>& permutation,
                                                                     const lGraph::xGraph& stored,
                                                                     const lGraph::xGraph& out);

} // namespace Integrals
} // namespace Caravel

#endif // INTEGRALTAGGER_H_
